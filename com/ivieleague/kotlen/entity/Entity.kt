package com.ivieleague.kotlen.entity

import java.util.*

/**
 * Created by josep on 9/5/2015.
 */
abstract class Entity<OWNER : Any>() {

    var id: Int = -1

    @Transient private val onStopFunctions: LinkedList<() -> Unit> = LinkedList()
    fun addOnStop(onStop: () -> Unit): Boolean = onStopFunctions.add(onStop)

    @Transient lateinit var owner: OWNER

    abstract fun setup(owner: OWNER)

    /**
     * Connects something (usually a module or part of this entity) to a collection, adding it into the collection now and removing it when the entity is stopped.
     */
    fun <T> connect(collection: MutableCollection<T>, item: T) {
        collection.add(item)
        onStopFunctions.add {
            collection.remove(item)
        }
    }

    fun start(owner: OWNER) {
        this.owner = owner
        setup(owner)
    }

    fun stop() {
        for (onStop in onStopFunctions) {
            onStop()
        }
    }

    open fun dispose(){}
}