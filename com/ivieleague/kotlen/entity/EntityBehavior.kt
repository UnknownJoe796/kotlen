package com.ivieleague.kotlen.entity

import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.collections.ConcurrentQueue
import com.ivieleague.kotlen.serialize.Serialize
import com.ivieleague.kotlen.serialize.readHashMap
import com.ivieleague.kotlen.serialize.writeHashMap
import java.util.*

/**
 * Created by josep on 9/5/2015.
 */
class EntityBehavior<OWNER : Any>(
        val owner: OWNER,
        override val order: Int = Int.MIN_VALUE,
        internal val entities: HashMap<Int, Entity<*>> = HashMap()
) : Behavior, Map<Int, Entity<*>> by entities {

    private val entityAddQueue: ConcurrentQueue<Entity<in OWNER>> = ConcurrentQueue()
    private val entityRemoveQueue: ConcurrentQueue<Int> = ConcurrentQueue()
    private var nextEntityId: Int = 0

    /**
     * Only call outside of iterating through them.
     */

    fun add(entity: Entity<in OWNER>) {
        entityAddQueue.add(entity)
    }

    fun remove(entity: Entity<*>) {
        entityRemoveQueue.add(entity.id)
    }

    fun remove(id: Int) {
        remove(entities[id] ?: return)
    }

    @Suppress("UNCHECKED_CAST")
    fun addTypeless(typelessEntity: Any) {
        val entity = typelessEntity as? Entity<in OWNER> ?: return
        add(entity)
    }

    @Suppress("UNCHECKED_CAST")
    fun removeTypeless(typelessEntity: Any) {
        val entity = typelessEntity as? Entity<in OWNER> ?: return
        remove(entity)
    }

    override fun start() {
    }

    override fun step(delta: Float) {
        for (id in entityRemoveQueue) {
            val entity = entities[id]
            entity?.stop()
            entity?.dispose()
            entities.remove(id)
        }
        entityRemoveQueue.clear()
        for (entity in entityAddQueue) {
            entities.put(nextEntityId, entity)
            entity.start(owner)
            entity.id = nextEntityId
            nextEntityId++
        }
        entityAddQueue.clear()
    }

    override fun render() {

    }

    override fun stop() {
        for (entity in entities.values) {
            entity.stop()
        }
    }



    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
        for (entity in entities.values) {
            entity.dispose()
        }
    }

    companion object {
        init {
            Serialize.objectSerializer<EntityBehavior<*>>({
                writeHashMap(it.entities)
            }, { old ->
                readHashMap(old.entities)
                old
            })
        }
    }

}