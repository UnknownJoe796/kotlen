package com.ivieleague.kotlen.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.language.xRange
import com.ivieleague.kotlen.language.yRange

/**
 * Created by josep on 1/4/2016.
 */
class ScrollBehavior(val bounds: Rectangle, val camera: OrthographicCamera) : Behavior {
    override val order: Int = Int.MAX_VALUE

    override fun start() {
    }


    val touchStart = Vector2()
    var lastTouched = false
    override fun step(delta: Float) {
        if (!lastTouched && Gdx.input.isTouched) {
            touchStart.set(Gdx.input.x.toFloat(), Gdx.input.y.toFloat())
        }
        if (Gdx.input.isTouched) {
            val deltaPosX = camera.combined.scaleX * Gdx.input.deltaX
            val deltaPosY = camera.combined.scaleY * Gdx.input.deltaY
            camera.position.add(-deltaPosX, -deltaPosY, 0f)
            camera.position.x = camera.position.x.coerceIn(bounds.xRange)
            camera.position.y = camera.position.y.coerceIn(bounds.yRange)
        }
        //TODO: Scroll wheel support?
        lastTouched = Gdx.input.isTouched
    }

    override fun render() {
    }

    override fun stop() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
    }

}