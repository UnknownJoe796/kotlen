package com.ivieleague.kotlen.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.math.Rectangle
import com.ivieleague.kotlen.behavior.ActionBehavior
import com.ivieleague.kotlen.rendering.unproject

/**
 * Created by josep on 1/2/2016.
 */
open class ButtonModule() {

    var bounds: Rectangle = Rectangle(0f, 0f, 32f, 32f)
    var state: State = State.NORMAL
    lateinit var camera: Camera
    lateinit var onClick: () -> Unit

    fun setup(camera: Camera, actionBehavior: ActionBehavior, onClick: () -> Unit) {
        this.camera = camera
        this.onClick = onClick
        actionBehavior.add {
            when (state) {
                State.NORMAL -> {
                    if (bounds.contains(camera.unproject(Gdx.input.x, Gdx.input.y))) {
                        state = State.OVER
                    }
                }
                State.OVER -> {
                    if (!bounds.contains(camera.unproject(Gdx.input.x, Gdx.input.y))) {
                        state = State.NORMAL
                    }
                    if (Gdx.input.isTouched) {
                        state = State.PRESSED
                    }
                }
                State.PRESSED -> {
                    if (!bounds.contains(camera.unproject(Gdx.input.x, Gdx.input.y))) {
                        state = State.NORMAL
                    }
                    if (!Gdx.input.isTouched) {
                        state = State.OVER
                        onClick()
                    }
                }
            }
        }
    }

    enum class State {
        NORMAL, OVER, PRESSED
    }
}