package com.ivieleague.kotlen.test.dots

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.ivieleague.kotlen.behavior.ActionBehavior
import com.ivieleague.kotlen.controls.ObjectControlMap
import com.ivieleague.kotlen.controls.bindTouchDown
import com.ivieleague.kotlen.controls.bindTouchPosition
import com.ivieleague.kotlen.entity.Entity
import com.ivieleague.kotlen.entity.EntityBehavior
import com.ivieleague.kotlen.networking.Client
import com.ivieleague.kotlen.networking.Packet
import com.ivieleague.kotlen.networking.Server
import com.ivieleague.kotlen.physics.BasicMotionBehavior
import com.ivieleague.kotlen.rendering.OrthographicCameraBehavior
import com.ivieleague.kotlen.rendering.ShapeRendererBehavior
import com.ivieleague.kotlen.rendering.unproject
import com.ivieleague.kotlen.serialize.DirectWrite
import com.ivieleague.kotlen.serialize.Serialize
import com.ivieleague.kotlen.simulation.Simulation
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * A simple test.
 * Created by josep on 10/3/2015.
 */
class DotTest() : Simulation() {
    var savedState: ByteArray? = null

    override val entities: EntityBehavior<DotTest> = EntityBehavior(this)

    class DotPlayer() : ObjectControlMap() {
        val gravityPointControl = value(Vector2())
        val gravityPoint by gravityPointControl
        val gravityOnControl = value(false)
        val gravityOn by gravityOnControl
    }
    val player = DotPlayer().apply{
        setup(controlBehavior, 0)
        gravityPointControl.bindTouchPosition()
        gravityOnControl.bindTouchDown()
    }

    var testServer: Server? = null
    var testClient: Client? = null

    init {

        Serialize.registerQuickCode<Dot>(11)
        Serialize.registerQuickCode<com.ivieleague.kotlen.test.dots.DotTest.DotPlayer>(0)
        Serialize.registerQuickCode<Boolean>(1)
        Serialize.registerQuickCode<com.badlogic.gdx.math.Vector2>(2)

    }

    val worldSize = Vector2(100f, 100f)

    val camera = add(OrthographicCameraBehavior(worldSize))
    val shapeRenderer = add(ShapeRendererBehavior(0, camera, true))

    val motion = add(BasicMotionBehavior(0))
    val action = add(ActionBehavior(1))

    init {
        val dot = Dot()
        dot.motion.x = 0f
        dot.motion.y = 0f
        dot.motion.vx = worldSize.x / 10
        dot.motion.vy = worldSize.y / 10
        entities.add(dot)
        for (i in 0..99) {
            val dot = Dot()
            dot.motion.x = worldSize.x * Math.random().toFloat()
            dot.motion.y = worldSize.y * Math.random().toFloat()
            dot.motion.vx = Math.random().toFloat() * 2 - 1
            dot.motion.vy = Math.random().toFloat() * 2 - 1
            entities.add(dot)
        }
        action.add {
            if (Gdx.input.isKeyPressed(Input.Keys.F7)) {
                val savedState = savedState ?: return@add
                val stream = ByteArrayInputStream(savedState)
                val wrapper = DataInputStream(stream)
                deserialize(wrapper)
            }
        }

        controlBehavior.keyDownListeners.add{
            when (it) {
                Input.Keys.F1 -> {
                    testServer = Server("127.0.0.1", 8239)
                }
                Input.Keys.F2 -> {
                    testClient = Client("127.0.0.1", 8239)
                }
                Input.Keys.F3 -> {
                    val stream = ByteArrayOutputStream()
                    val wrapper = DataOutputStream(stream)
                    Serialize.writeUntyped(wrapper, player)
                }
                Input.Keys.NUM_1 -> {
                    testServer?.send("test")
                }
                Input.Keys.NUM_2 -> {
                    val stream = ByteArrayOutputStream()
                    val wrapper = DataOutputStream(stream)
                    serialize(wrapper)
                    savedState = stream.toByteArray()
                    println(savedState!!.size.toString())
                    Serialize.printNeededQuickCodes()
                    testServer?.send(Packet(0.0, savedState!!))
                }
                Input.Keys.NUM_3 -> {
                    println(controlBehavior.controlUpdates.size)
                }
                Input.Keys.F5 -> {
                    //println("save")
                    val stream = ByteArrayOutputStream()
                    val wrapper = DataOutputStream(stream)
                    serialize(wrapper)
                    savedState = stream.toByteArray()
                    println(savedState!!.size.toString())
                    Serialize.printNeededQuickCodes()
                }
                else -> return@add false
            }
            true
        }
    }

    override fun step(delta: Float) {
        val output = testClient?.output
        if (output != null) while (!output.isEmpty()) {
            val newItem = output.poll()
            if (newItem is Packet) {
                val savedState = newItem.byteArray
                val stream = ByteArrayInputStream(savedState)
                val wrapper = DataInputStream(stream)
                deserialize(wrapper)
            }
        }
        super.step(delta)
    }

    class Dot() : Entity<DotTest>() {
        @DirectWrite val motion = BasicMotionBehavior.Module()
        override fun setup(owner: DotTest) {
            connect(owner.motion, motion)
            connect(owner.shapeRenderer) {
                color = if (owner.player.gravityOn) Color.RED else Color.WHITE
                circle(motion.x, motion.y, 2f, 12)
            }
            connect(owner.action) {
                if (owner.player.gravityOn) {
                    motion.attract(it, owner.camera.unproject(owner.player.gravityPoint), 1f, 1000f, 100f)
                }
            }
        }
    }
}