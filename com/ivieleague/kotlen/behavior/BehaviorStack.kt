package com.ivieleague.kotlen.behavior

import com.badlogic.gdx.Gdx
import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by josep on 1/5/2016.
 */
class BehaviorStack() : Stack<Behavior>(), Behavior {
    override fun onBackPressed(action: () -> Unit) {
        if (size <= 1) action()
        else lastOrNull()?.onBackPressed {
            pop()
        }
    }

    var current: Behavior? = null

    override val order: Int get() = 0

    override fun start() {
        current = lastOrNull()
        current?.resize(Gdx.graphics.width, Gdx.graphics.height)
        current?.start()
    }

    override fun step(delta: Float) {
        if (lastOrNull() != current) {
            //swap
            current?.stop()
            current?.dispose()
            current = lastOrNull()
            current?.start()
            current?.resize(Gdx.graphics.width, Gdx.graphics.height)
        }
        current?.step(delta)
    }

    override fun render() {
        current?.render()
    }

    override fun stop() {
        current?.stop()
    }

    override fun resize(width: Int, height: Int) {
        current?.resize(width, height)
    }

    override fun dispose() {
        current?.dispose()
    }

}