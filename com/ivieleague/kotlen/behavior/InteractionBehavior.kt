package com.ivieleague.kotlen.behavior

import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by josep on 8/17/2015.
 */
abstract class InteractionBehavior<A, B>(
        override val order: Int
) : Behavior {
    val left: ArrayList<A> = ArrayList()
    val right: ArrayList<B> = ArrayList()

    override fun start() {
    }

    override fun step(delta: Float) {
        for (a in left) {
            for (b in right) {
                interact(a, b)
            }
        }
    }

    override fun render() {
    }

    abstract fun interact(left: A, right: B)

    override fun stop() {
    }
}