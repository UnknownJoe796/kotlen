package com.ivieleague.kotlen.behavior

import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by josep on 8/17/2015.
 */
class InternalInteractionBehavior<T>(
        override val order: Int,
        val interact: (delta: Float, first: T, second: T) -> Unit
) : Behavior, MutableList<T> by ArrayList() {

    override fun step(delta: Float) {
        val outer = listIterator()
        for (left in outer) {
            val inner = listIterator(outer.nextIndex())
            for (right in inner) {
                interact(delta, left, right)
            }
        }
    }

    override fun start() {
    }

    override fun render() {

    }

    override fun stop() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
    }
}