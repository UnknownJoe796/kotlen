package com.ivieleague.kotlen.behavior

import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.entity.Entity
import com.ivieleague.kotlen.entity.EntityBehavior
import java.util.*

/**
 * Created by josep on 8/15/2015.
 */
inline fun <reified OTHER : Entity<*>> InteractBehavior(
        entities: EntityBehavior<*>, order: Int
): InteractBehaviorImpl<OTHER> {
    return object : InteractBehaviorImpl<OTHER>(order) {

        override fun step(delta: Float) {
            for (action in this) {
                for (entity: Entity<*> in entities.values) {
                    if (entity is OTHER) {
                        action(delta, entity)
                    }
                }
            }
        }
    }
}

abstract class InteractBehaviorImpl<OTHER>(
        override val order: Int = 0
) : Behavior, MutableList<(delta: Float, other: OTHER) -> Unit> by ArrayList() {
    override fun resize(width: Int, height: Int) {
    }

    override fun start() {
    }

    override fun stop() {
    }

    override fun dispose() {
    }

    override fun render() {

    }
}