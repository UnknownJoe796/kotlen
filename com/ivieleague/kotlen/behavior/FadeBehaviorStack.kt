package com.ivieleague.kotlen.behavior

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.rendering.ShapeRendererBehavior
import java.util.*

/**
 * Created by josep on 1/5/2016.
 */
class FadeBehaviorStack() : Stack<Behavior>(), Behavior {
    override fun onBackPressed(action: () -> Unit) {
        if (size <= 1) action()
        else lastOrNull()?.onBackPressed {
            pop()
        }
    }

    var current: Behavior? = null
    var fadeLevel: Float = 0f
    var fadeSpeed: Float = 5f
    var fadeColor: Color = Color.WHITE

    override val order: Int get() = 0
    val transitionRenderer = ShapeRendererBehavior(0, null, true)

    override fun start() {
        current = lastOrNull()
        current?.resize(Gdx.graphics.width, Gdx.graphics.height)
        current?.start()
        transitionRenderer.start()
        transitionRenderer.add {
            Gdx.gl.glEnable(GL20.GL_BLEND)
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            color = fadeColor.cpy().apply { a = fadeLevel }
            rect(0f, 0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
        }
    }

    override fun step(delta: Float) {
        if (lastOrNull() != current) {
            fadeLevel += fadeSpeed * delta
            if (fadeLevel >= 1f) {
                fadeLevel = 1f
                //swap
                current?.stop()
                current?.dispose()
                current = lastOrNull()
                current?.start()
                current?.resize(Gdx.graphics.width, Gdx.graphics.height)
            }
        } else {
            fadeLevel = (fadeLevel - fadeSpeed * delta).coerceAtLeast(0f)
        }
        current?.step(delta)
    }

    override fun render() {
        current?.render()
        transitionRenderer.render()
    }

    override fun stop() {
        current?.stop()
        transitionRenderer.stop()
    }

    override fun resize(width: Int, height: Int) {
        current?.resize(width, height)
        transitionRenderer.resize(width, height)
    }

    override fun dispose() {
        current?.dispose()
        transitionRenderer.dispose()
    }

}