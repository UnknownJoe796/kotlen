package com.ivieleague.kotlen.behavior

import com.badlogic.gdx.Gdx
import com.ivieleague.kotlen.Behavior

/**
 * Created by josep on 1/2/2016.
 */
open class BehaviorSwitcher(override val order: Int = 0) : Behavior {

    lateinit var current: Behavior
    var nextBehavior: Behavior? = null

    fun configure(firstBehavior: Behavior): BehaviorSwitcher {
        current = firstBehavior
        return this
    }

    fun swap(nextBehavior: Behavior) {
        this.nextBehavior = nextBehavior
    }

    override fun start() {
        current.start()
    }

    override fun step(delta: Float) {
        if (nextBehavior != null) {
            //swap
            current.stop()
            current.dispose()
            current = nextBehavior!!
            current.resize(Gdx.graphics.width, Gdx.graphics.height)
            current.start()
            nextBehavior = null
        }
        current.step(delta)
    }

    override fun render() {
        current.render()
    }

    override fun stop() {
        current.stop()
        nextBehavior = null
    }

    override fun resize(width: Int, height: Int) {
        current.resize(width, height)
    }

    override fun dispose() {
        current.dispose()
    }
}