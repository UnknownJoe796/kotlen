package com.ivieleague.kotlen.behavior

import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by josep on 8/10/2015.
 */
class AlarmBehavior(
        override val order: Int
) : Behavior, MutableCollection<AlarmBehavior.Module> by ArrayList() {

    override fun step(delta: Float) {
        for (module in this) {
            if (!module.active) continue
            module.time -= delta
            if (module.time <= 0) {
                module.action()
                module.active = false
            }
        }
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun start() {
    }

    override fun render() {

    }

    override fun stop() {
    }

    override fun dispose() {
    }

    data class Module(
            val startTime: Float,
            var active: Boolean,
            val action: () -> Unit
    ) {
        var time: Float = startTime
        fun reset() {
            active = true
            time = startTime
        }

        fun reset(time: Float) {
            active = true
            this.time = time
        }
    }
}