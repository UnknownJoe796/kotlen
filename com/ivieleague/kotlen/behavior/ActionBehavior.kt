package com.ivieleague.kotlen.behavior

import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by josep on 8/15/2015.
 */
class ActionBehavior(
        override val order: Int
) : Behavior, MutableList<(delta: Float) -> Unit> by ArrayList() {
    override fun start() {
    }

    override fun step(delta: Float) {
        forEach { it(delta) }
    }

    override fun render() {
    }

    override fun stop() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
    }
}