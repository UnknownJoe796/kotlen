package com.ivieleague.kotlen.behavior

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.ivieleague.kotlen.Behavior

/**
 * Created by josep on 10/23/2015.
 */
class BehaviorToScreen(val behavior: Behavior) : Screen {
    override fun show() {
        Gdx.input.isCatchBackKey = true
        behavior.start()
    }

    override fun pause() {
    }

    override fun resize(width: Int, height: Int) {
        behavior.resize(width, height)
    }

    override fun hide() {
        behavior.stop()
    }

    override fun render(delta: Float) {
        behavior.step(delta)
        behavior.render()
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            behavior.onBackPressed {
                Gdx.app.exit()
            }
        }
    }

    override fun resume() {
    }

    override fun dispose() {
        behavior.dispose()
    }
}

fun Behavior.toScreen(): BehaviorToScreen = BehaviorToScreen(this)