package com.ivieleague.kotlen.map

import com.badlogic.gdx.maps.MapObject
import com.badlogic.gdx.maps.objects.*
import com.badlogic.gdx.math.Shape2D

/**
 * Created by josep on 1/8/2016.
 */

val MapObject.shape: Shape2D get() {
    return when (this) {
        is PolygonMapObject -> polygon
        is PolylineMapObject -> polyline
        is CircleMapObject -> circle
        is EllipseMapObject -> ellipse
        is RectangleMapObject -> rectangle
        else -> {
            throw IllegalArgumentException()
        }
    }
}