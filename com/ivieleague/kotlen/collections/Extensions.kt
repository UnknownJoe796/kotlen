package com.ivieleague.kotlen.collections

/**
 * Created by josep on 11/28/2015.
 */
public fun <T : Comparable<T>> MutableList<T>.orderedAdd(element: T): Boolean {
    var itr = listIterator();
    while (true) {
        if (itr.hasNext() == false) {
            itr.add(element);
            return (true);
        }

        var elementInList = itr.next();
        if (elementInList.compareTo(element) > 0) {
            itr.previous();
            itr.add(element);
            return (true);
        }
    }
}