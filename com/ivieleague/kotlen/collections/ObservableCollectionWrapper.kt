package com.ivieleague.kotlen.collections

/**
 * Created by josep on 9/7/2015.
 */
class ObservableCollectionWrapper<E>(
        val collection: MutableCollection<E>,
        val onAdd: (E) -> Unit,
        val onRemove: (E) -> Unit
) : MutableCollection<E> {
    override val size: Int get() = collection.size

    override fun iterator(): MutableIterator<E> = collection.iterator()
    override fun add(element: E): Boolean {
        onAdd(element)
        return collection.add(element)
    }

    @Suppress("UNCHECKED_CAST")
    override fun remove(element: E): Boolean {
        onRemove(element)
        return collection.remove(element)
    }

    override fun addAll(elements: Collection<E>): Boolean {
        for (e in elements) {
            onAdd(e)
        }
        return collection.addAll(elements)
    }

    @Suppress("UNCHECKED_CAST")
    override fun removeAll(elements: Collection<E>): Boolean {
        for (e in elements) {
            onRemove(e)
        }
        return collection.removeAll(elements)
    }

    override fun retainAll(elements: Collection<E>): Boolean {
        throw UnsupportedOperationException()
    }

    override fun clear() {
        for (e in collection) {
            onRemove(e)
        }
        collection.clear()
    }

    override fun isEmpty(): Boolean = collection.isEmpty()
    override fun contains(element: E): Boolean = collection.contains(element)
    override fun containsAll(elements: Collection<E>): Boolean = collection.containsAll(elements)

}