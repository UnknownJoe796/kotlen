package com.ivieleague.kotlen.collections

/**
 * Created by joseph on 6/30/15.
 */
interface DelayedSet<T> : MutableSet<T>, Iterable<T>
{
    fun update()
}