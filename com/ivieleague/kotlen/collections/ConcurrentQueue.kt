package com.ivieleague.kotlen.collections

import java.util.*

/**
 * Created by joseph on 7/8/15
 */
class ConcurrentQueue<T> : Queue<T>
{
    override fun removeAll(elements: Collection<T>): Boolean {
        for (e in elements) {
            remove(e)
        }
        return true
    }

    private var tail: Node<T>? = null;
    private var head: Node<T>? = null;

    override fun peek(): T?
    {
        return head?.data
    }

    override fun element(): T
    {
        val e = peek() ?: throw NoSuchElementException()
        return e
    }

    override fun offer(e: T): Boolean
    {
        add(e)
        return true
    }

    override fun poll(): T?
    {
        if (head != null)
        {
            val data = head!!.data
            head = head!!.after
            return data
        }
        else
        {
            return null
        }
    }

    override fun remove(): T
    {
        val e = poll() ?: throw NoSuchElementException()
        return e
    }

    override fun add(element: T): Boolean
    {
        val new = Node(element)
        new.before = tail
        tail?.after = new
        tail = new
        if (head == null)
        {
            head = new
        }
        return true
    }

    override fun iterator(): MutableIterator<T>
    {
        return Iterator(this)
    }

    override fun remove(element: T): Boolean
    {
        val it = iterator()
        while (it.hasNext())
        {
            val data = it.next()
            if (data == element)
            {
                it.remove()
                return true
            }
        }
        return false
    }

    override fun addAll(elements: Collection<T>): Boolean
    {
        for (e in elements)
        {
            add(e)
        }
        return true
    }

    override fun clear()
    {
        head = null
        tail = null
    }

    override val size: Int
        get()
    {
        var i: Int = 0
        for (e in this)
        {
            i++
        }
        return i
    }

    override fun isEmpty(): Boolean
    {
        return head == null
    }

    override fun contains(element: T): Boolean
    {
        for (e in this)
        {
            if (e == element)
            {
                return true
            }
        }
        return false
    }

    override fun containsAll(elements: Collection<T>): Boolean
    {
        throw UnsupportedOperationException()
    }

    override fun retainAll(elements: Collection<T>): Boolean {
        val it = iterator()
        var anyRemoved = false
        while (it.hasNext()) {
            val data = it.next()
            for (e in elements) {
                if (data == e) {
                    it.remove()
                    anyRemoved = true
                    break
                }
            }
        }
        return anyRemoved
    }

    fun cloneableIterator(): Iterator<T> = Iterator(this)

    class Iterator<T>(queue: ConcurrentQueue<T>) : MutableIterator<T>
    {
        val queue: ConcurrentQueue<T> = queue
        private var nextNode: Node<T>? = queue.head
        private var currentNode: Node<T>? = null
        override fun remove()
        {
            if (currentNode == null) throw NoSuchElementException()
            val current = currentNode!!
            if (current == queue.head)
            {
                queue.head = current.after
            } else {
                val before = current.before!!
                before.after = current.after
            }
            if (current == queue.tail)
            {
                queue.tail = current.before
            } else {
                val after = current.after!!
                after.before = current.before
            }
            current.after = null
            current.before = null
        }

        override fun next(): T
        {
            if (nextNode == null) throw NoSuchElementException()
            val data = nextNode!!.data
            currentNode = nextNode
            nextNode = currentNode!!.after
            return data
        }

        override fun hasNext(): Boolean
        {
            return nextNode != null
        }

        fun clone(): Iterator<T> {
            val new = Iterator(queue)
            new.nextNode = nextNode
            return new
        }

    }

    private class Node<T>(data: T)
    {
        val data: T = data
        var before: Node<T>? = null;
        var after: Node<T>? = null;
    }
}