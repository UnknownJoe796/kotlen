package com.ivieleague.kotlen.collections

import java.util.*

/**
 * A set that is very particular about the timing of changes.
 * It adds in the order you add them.
 * It clears in the reverse of that order.
 * It iterates in the order they were added.
 * It doesn't change until you call update().
 * Created by joseph on 6/25/15.
 */
class UnsortedDelayedSet<T>(onAdd: (T) -> Unit = {}, onRemove: (T) -> Unit = {}) : DelayedSet<T>
{

    private var addList: ConcurrentQueue<T> = ConcurrentQueue()
    private var removeList: ConcurrentQueue<T> = ConcurrentQueue()
    private var mainSet: MutableSet<T> = LinkedHashSet()
    private var removeAllOnNext: Boolean = false

    private val onAdd: (T) -> Unit = onAdd
    private val onRemove: (T) -> Unit = onRemove

    override fun iterator(): MutableIterator<T> = mainSet.iterator()

    /**
     * Runs all of the queued instructions, removing items first and then adding items.
     */
    override fun update()
    {
        if (removeAllOnNext)
        {
            val reverseList = ArrayList(mainSet)
            Collections.reverse(reverseList)
            for (item in reverseList)
            {
                onRemove(item)
            }
            mainSet.clear()
            removeAllOnNext = false
        }

        for (data in removeList)
        {
            if (mainSet.remove(data))
            {
                onRemove(data)
            }
        }
        removeList.clear()
        for (data in addList)
        {
            if (mainSet.add(data))
            {
                onAdd(data)
            }
        }
        addList.clear()

    }

    /**
     * Queues the item for addition in the next update.
     */
    override fun add(element: T): Boolean
    {
        if (mainSet.contains(element)) return false
        return addList.add(element)
    }

    /**
     * Queues the item for removal in the next update.
     * If it's in the add queue, it will remove it from the add queue instead.
     */
    override fun remove(element: T): Boolean
    {
        if (addList.contains(element))
        {
            addList.remove(element)
            return true
        }
        if (!mainSet.contains(element)) return false
        val item = element
        return removeList.add(item)
    }

    override fun contains(element: T): Boolean
    {
        return mainSet.contains(element)
    }

    /**
     * Overrides all other instructions in next update for clearing all current items, clearing the queues as well.
     */
    override fun clear()
    {
        removeAllOnNext = true
        addList.clear()
        removeList.clear()
    }

    override fun addAll(elements: Collection<T>): Boolean
    {
        var result = false
        for (item in elements)
        {
            if (add(item)) result = true
        }
        return result
    }

    override fun removeAll(elements: Collection<T>): Boolean
    {
        var result = false
        for (item in elements)
        {
            if (remove(item)) result = true
        }
        return result
    }

    override fun retainAll(elements: Collection<T>): Boolean
    {
        var result = false
        for (item in mainSet)
        {
            if (!elements.contains(item))
            {
                if (remove(item)) result = true
            }
        }
        return result
    }

    override val size: Int get() = mainSet.size

    override fun isEmpty(): Boolean
    {
        return mainSet.isEmpty()
    }

    override fun containsAll(elements: Collection<T>): Boolean
    {
        return mainSet.containsAll(elements)
    }
}