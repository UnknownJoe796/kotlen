package com.ivieleague.kotlen.physics.box2d

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.*
import com.badlogic.gdx.physics.box2d.*

/**
 * Created by joseph on 6/29/15.
 */

var Body.x: Float
    get() = position.x
    set(value) = setTransform(value, y, angle)
var Body.y: Float
    get() = position.y
    set(value) = setTransform(x, value, angle)
var Body.vx: Float
    get() = linearVelocity.x
    set(value) = setLinearVelocity(value, vy)
var Body.vy: Float
    get() = linearVelocity.y
    set(value) = setLinearVelocity(vx, value)

fun Body.getTransformMatrix(matrix: Matrix4): Matrix4 {
    val transform = transform
    matrix.idt()
    matrix.translate(transform.position.x, transform.position.y, 0f)
    matrix.rotate(0f, 0f, 1f, (transform.rotation * 180 / Math.PI).toFloat())
    return matrix
}

inline fun World.makeBody(init: BodyDef.() -> Unit): Body {
    val def = BodyDef()
    def.init()
    return createBody(def)
}

inline fun Body.addFixture(init: FixtureDef.() -> Unit): Body {
    val def = FixtureDef()
    def.init()
    createFixture(def)
    return this
}

inline fun Body.makeFixture(init: FixtureDef.() -> Unit): Fixture {
    val def = FixtureDef()
    def.init()
    return createFixture(def)
}

fun Body.makeDef(): BodyDef {
    val def = BodyDef()
    def.active = isActive
    def.allowSleep = isSleepingAllowed
    def.angle = angle
    def.angularDamping = angularDamping
    def.angularVelocity = angularVelocity
    def.awake = isAwake
    def.bullet = isBullet
    def.fixedRotation = isFixedRotation
    def.gravityScale = gravityScale
    def.linearDamping = linearDamping
    def.linearVelocity.set(linearVelocity)
    def.position.set(position)
    def.type = type
    return def
}

fun Fixture.makeDef(): FixtureDef {
    val def = FixtureDef()
    def.density = density
    def.filter.categoryBits = filterData.categoryBits
    def.filter.groupIndex = filterData.groupIndex
    def.filter.maskBits = filterData.maskBits
    def.friction = friction
    def.isSensor = isSensor
    def.restitution = restitution
    def.shape = shape
    return def
}

fun Body.makeFixtureDefs(): Array<FixtureDef> {
    val fixtures = fixtureList
    val defs = Array(fixtures.size) { index -> fixtures[index].makeDef() }
    return defs
}

fun Body.makeFixtures(defs: Array<FixtureDef>): Array<Fixture> {
    return Array(defs.size()) { index -> createFixture(defs[index]) }
}

val World.bodies: com.badlogic.gdx.utils.Array<Body?>
    get() {
        val bodies = com.badlogic.gdx.utils.Array<Body?>(bodyCount)
        getBodies(bodies)
        return bodies
    }

fun World.explosion(explosionCenter: Vector2, strength: Float) {
    for (body in bodies) {
        if (body == null
                || body.type != BodyDef.BodyType.DynamicBody) {
            continue
        }

        val dx = body.x - explosionCenter.x
        val dy = body.y - explosionCenter.y

        var dist = dx * dx + dy * dy
        if (dist < .05f) {
            dist = .05f
        }
        val scale = strength / dist
        body.applyLinearImpulse(
                dx * scale,
                dy * scale,
                body.x,
                body.y,
                true
        )
    }
}

fun ShapeRenderer.drawBodyFilled(body: Body, color: Color) {
    body.getTransformMatrix(transformMatrix)
    updateMatrices()
    setColor(color)
    body.fixtureList.forEach {
        val shape = it.shape
        when (shape) {
            is PolygonShape -> {
                if (shape.vertexCount >= 3) {
                    val vertexA = Vector2()
                    shape.getVertex(0, vertexA)
                    val vertexB = Vector2()
                    val vertexC = Vector2()
                    repeat(shape.vertexCount - 2) {
                        shape.getVertex(it + 1, vertexB)
                        shape.getVertex(it + 2, vertexC)
                        triangle(vertexA.x, vertexA.y, vertexB.x, vertexB.y, vertexC.x, vertexC.y)
                    }
                }
            }
            is CircleShape -> {
                circle(shape.position.x, shape.position.y, shape.radius, 12)
            }
        }
    }
}

fun ShapeRenderer.drawBodyOutline(body: Body, color: Color) {
    body.getTransformMatrix(transformMatrix)
    updateMatrices()
    setColor(color)
    body.fixtureList.forEach {
        val shape = it.shape
        when (shape) {
            is PolygonShape -> {
                val vertex = Vector2()
                polygon(FloatArray(shape.vertexCount * 2) {
                    shape.getVertex(it / 2, vertex)
                    if (it % 2 == 0) vertex.x
                    else vertex.y
                })
                /*polygon(shape.vertexCount){ index, point ->
                    shape.getVertex(index, point)
                }*/
            }
            is CircleShape -> {
                circle(shape.position.x, shape.position.y, shape.radius, 12)
            }
        }
    }
}

val Shape.center: Vector2 get() {
    return when (this) {
        is CircleShape -> this.position
        is PolygonShape -> {
            val avg = Vector2()
            val temp = Vector2()
            for (i in 0..vertexCount - 1) {
                getVertex(i, temp)
                avg.x += temp.x
                avg.y += temp.y
            }
            avg.x /= vertexCount
            avg.y /= vertexCount
            avg
        }
        else -> {
            throw IllegalArgumentException()
        }
    }
}

fun Shape2D.toBox2D(): Shape {
    return when (this) {
        is Polygon -> toBox2D()
        is Circle -> toBox2D()
        is Polyline -> toBox2D()
        is Ellipse -> toBox2D(12)
        is Rectangle -> toBox2D()
        else -> throw IllegalArgumentException()
    }
}

fun Polygon.toBox2D(): PolygonShape {
    val shape = PolygonShape()
    shape.set(vertices)
    return shape
}

fun Circle.toBox2D(): CircleShape {
    val shape = CircleShape()
    shape.position = Vector2(x, y)
    shape.radius = radius
    return shape
}

fun Polyline.toBox2D(): ChainShape {
    val shape = ChainShape()
    shape.createChain(vertices)
    return shape
}

fun Ellipse.toBox2D(sides: Int = 12): Shape {
    return if (width == height) {
        val shape = CircleShape()
        shape.radius = width / 2
        shape
    } else {
        val shape = PolygonShape()
        val array = FloatArray(sides * 2)
        repeat(sides) {
            val radians = it / sides * Math.PI * 2
            array[it * 2] = Math.cos(radians).toFloat() * width
            array[it * 2 + 1] = -Math.sin(radians).toFloat() * height
        }
        shape.set(array)
        shape
    }
}

fun Rectangle.toBox2D(): PolygonShape {
    val shape = PolygonShape()
    val hx = width / 2
    val hy = height / 2
    shape.setAsBox(hx, hy, Vector2(x + hx, y + hy), 0f)
    return shape
}