package com.ivieleague.kotlen.physics.box2d

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.ivieleague.kotlen.entity.Entity
import java.util.*

/**
 * Created by josep on 1/8/2016.
 */
open class Box2DModule(entity: Entity<*>?) {

    @Transient var body: Body? = null
    @Transient var entity: Entity<*>? = entity
    @Transient open var listener: Box2DBehavior.ContactListener? = null

    val bodyDef: BodyDef = BodyDef()
    @Transient val fixtureDefs: ArrayList<FixtureDefPlus> = ArrayList()

    fun bodyDef(setup: BodyDef.() -> Unit): Box2DModule {
        bodyDef.setup()
        return this
    }

    fun fixtureDef(setup: FixtureDefPlus.() -> Unit): FixtureDefPlus {
        val def = FixtureDefPlus()
        def.setup()
        fixtureDefs.add(def)
        if (body != null) {
            body!!.createFixture(def)
        }
        return def
    }

    fun makeBody(world: World): Body {
        body = world.createBody(bodyDef).apply {
            for (def in fixtureDefs) {
                def.make(this)
            }
        }
        return body!!
    }

    fun unmakeBody(world: World) {
        if (body != null) {
            world.destroyBody(body)
        }
        for (def in fixtureDefs) {
            def.unmake()
        }
    }

    var x: Float
        get() = body?.position?.x ?: bodyDef.position.x
        set(value) {
            if (body != null) {
                body!!.setTransform(value, body!!.y, body!!.angle)
            } else {
                bodyDef.position.x = value
            }
        }

    var y: Float
        get() = body?.position?.y ?: bodyDef.position.y
        set(value) {
            if (body != null) {
                body!!.setTransform(body!!.x, value, body!!.angle)
            } else {
                bodyDef.position.y = value
            }
        }

    var position: Vector2
        get() = body?.position ?: bodyDef.position
        set(value) {
            if (body != null) {
                body!!.setTransform(value, body!!.angle)
            } else {
                bodyDef.position.set(value)
            }
        }

    var angle: Float
        get() = body?.angle ?: bodyDef.angle
        set(value) {
            if (body != null) {
                body!!.setTransform(body!!.position.x, body!!.position.y, value)
            } else {
                bodyDef.angle = value
            }
        }

    var vx: Float
        get() = body?.linearVelocity?.x ?: bodyDef.linearVelocity.x
        set(value) {
            if (body != null) {
                body!!.setLinearVelocity(value, body!!.linearVelocity.y)
            } else {
                bodyDef.linearVelocity.x = value
            }
        }

    var vy: Float
        get() = body?.linearVelocity?.y ?: bodyDef.linearVelocity.y
        set(value) {
            if (body != null) {
                body!!.setLinearVelocity(body!!.linearVelocity.x, value)
            } else {
                bodyDef.linearVelocity.y = value
            }
        }

    var velocity: Vector2
        get() = body?.linearVelocity ?: bodyDef.linearVelocity
        set(value) {
            if (body != null) {
                body!!.linearVelocity = value
            } else {
                bodyDef.linearVelocity.set(value)
            }
        }

    var angularVelocity: Float
        get() = body?.angularVelocity ?: bodyDef.angularVelocity
        set(value) {
            if (body != null) {
                body!!.angularVelocity = value
            } else {
                bodyDef.angularVelocity = value
            }
        }
}