package com.ivieleague.kotlen.physics.box2d

import com.badlogic.gdx.math.Polyline
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by joseph on 6/29/15.
 */
class Box2DBehavior(
        override val order: Int = 0
) : Behavior, MutableCollection<Box2DModule>, ContactListener {

    init{
        Box2D.init()
    }

    var world: World? = null

    val map: HashMap<Body, Box2DModule> = HashMap()

    var timeStep: Float = -1f
    var positionIterations: Int = 5
    var velocityIterations: Int = 5

    //Collection
    override fun add(element: Box2DModule): Boolean {
        val world = world ?: throw IllegalStateException()
        map.put(element.makeBody(world), element)
        return true
    }

    override fun render() {

    }

    override fun remove(element: Box2DModule): Boolean {
        val world = world ?: return false
        element.unmakeBody(world)
        val toRemove = element.body
        if (toRemove != null) {
            map.remove(toRemove)
            return true
        }
        return false
    }

    override fun iterator(): MutableIterator<Box2DModule> = map.values().iterator()
    override val size: Int get() = map.size
    override fun isEmpty(): Boolean = map.isEmpty()
    override fun contains(o: Box2DModule): Boolean = map.containsValue(o)
    override fun addAll(c: Collection<Box2DModule>): Boolean {
        c.forEach { add(it) }
        return true
    }

    override fun removeAll(elements: Collection<Box2DModule>): Boolean {
        var success = false
        elements.forEach { success = remove(it) || success }
        return true
    }

    override fun retainAll(elements: Collection<Box2DModule>): Boolean {
        throw UnsupportedOperationException()
    }

    override fun clear() {
        val world = world ?: return
        for (module in this) {
            module.unmakeBody(world)
        }
        map.clear()
    }

    override fun containsAll(elements: Collection<Box2DModule>): Boolean {
        for (module in elements) {
            if (!contains(module)) {
                return false
            }
        }
        return true
    }

    fun rayCast(pointA: Vector2, pointB: Vector2, callback: (module: Box2DModule?, fixture: Fixture, point: Vector2, normal: Vector2, fraction: Float) -> Float) {
        world?.rayCast(RaycastListener(callback), pointA, pointB)
    }

    fun rayCast(pointA: Vector2, pointB: Vector2, callback: (module: Box2DModule?) -> Unit) {
        world?.rayCast(RaycastListenerSimple(callback), pointA, pointB)
    }

    //lifecycle
    override fun start() {
        Box2D.init()
        world = World(Vector2.Zero, true)
        world!!.setContactListener(this)
    }

    override fun step(delta: Float) {
        world?.step(if (timeStep <= 0f) delta else timeStep, velocityIterations, positionIterations)
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun stop() {
        world?.dispose()
        world = null
    }

    override fun dispose() {
    }

    //contact listener
    override fun postSolve(contact: Contact, impulse: ContactImpulse) {
        val moduleA = map[contact.fixtureA.body]
        val moduleB = map[contact.fixtureB.body]
        moduleA?.listener?.postSolve(moduleB!!, contact, impulse)
        moduleB?.listener?.postSolve(moduleA!!, contact, impulse)
    }

    override fun beginContact(contact: Contact) {
        val moduleA = map[contact.fixtureA.body]
        val moduleB = map[contact.fixtureB.body]
        moduleA?.listener?.beginContact(moduleB!!, contact)
        moduleB?.listener?.beginContact(moduleA!!, contact)
    }

    override fun endContact(contact: Contact) {
        val moduleA = map[contact.fixtureA.body]
        val moduleB = map[contact.fixtureB.body]
        moduleA?.listener?.endContact(moduleB!!, contact)
        moduleB?.listener?.endContact(moduleA!!, contact)
    }

    override fun preSolve(contact: Contact, oldManifold: Manifold) {
        val moduleA = map[contact.fixtureA.body]
        val moduleB = map[contact.fixtureB.body]
        moduleA?.listener?.preSolve(moduleB!!, contact, oldManifold)
        moduleB?.listener?.preSolve(moduleA!!, contact, oldManifold)
    }

    interface ContactListener {
        fun beginContact(other: Box2DModule, contact: Contact)
        fun endContact(other: Box2DModule, contact: Contact)
        fun preSolve(other: Box2DModule, contact: Contact, oldManifold: Manifold)
        fun postSolve(other: Box2DModule, contact: Contact, impulse: ContactImpulse)
    }

    inner class RaycastListener(
            val action: (module: Box2DModule?, fixture: Fixture, point: Vector2, normal: Vector2, fraction: Float) -> Float
    ) : RayCastCallback {
        override fun reportRayFixture(fixture: Fixture, point: Vector2, normal: Vector2, fraction: Float): Float {
            return action(map[fixture.body], fixture, point, normal, fraction)
        }
    }

    inner class RaycastListenerSimple(
            val action: (module: Box2DModule?) -> Unit
    ) : RayCastCallback {
        override fun reportRayFixture(fixture: Fixture, point: Vector2, normal: Vector2, fraction: Float): Float {
            action(map[fixture.body])
            return 1f
        }
    }

    companion object{
        fun borderModule(worldSize: Vector2): Box2DModule {
            return Box2DModule(null).apply {
                bodyDef {
                    type = BodyDef.BodyType.StaticBody
                }
                fixtureDef {
                    shape2D = Polyline(floatArrayOf(
                            0f, 0f,
                            worldSize.x, 0f,
                            worldSize.x, worldSize.y,
                            0f, worldSize.y,
                            0f, 0f
                    ))
                }
            }
        }
    }
}