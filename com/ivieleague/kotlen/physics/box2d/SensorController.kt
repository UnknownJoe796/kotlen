package com.ivieleague.kotlen.physics.box2d

import com.badlogic.gdx.math.Polygon
import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.Manifold
import com.ivieleague.kotlen.language.border
import java.util.*

/**
 * Created by josep on 1/8/2016.
 */
class SensorController(module: Box2DModule) : Box2DBehavior.ContactListener {
    init {
        module.listener = this
    }

    @Transient var chainFixtureDef: FixtureDefPlus = module.fixtureDef {
        isSensor = true
    }
    @Transient var polyFixtureDef: FixtureDefPlus = module.fixtureDef {
        isSensor = true
    }

    private var privatePolygon: Polygon? = null
    var polygon: Polygon
        get() = privatePolygon ?: throw IllegalStateException()
        set(value) {
            chainFixtureDef.shape2D = value.border()
            polyFixtureDef.shape2D = value
            privatePolygon = value
        }

    @Transient val polyContact: HashSet<Box2DModule> = HashSet()
    @Transient val chainContacts: HashMap<Box2DModule, Int> = HashMap()

    @Transient val partiallyInside: HashSet<Box2DModule> = HashSet()
    @Transient val fullyInside: HashSet<Box2DModule> = HashSet()

    override fun beginContact(other: Box2DModule, contact: Contact) {
        if (contact.fixtureA == polyFixtureDef.fixture || contact.fixtureB == polyFixtureDef.fixture) {
            polyContact.add(other)
        } else if (contact.fixtureA == chainFixtureDef.fixture || contact.fixtureB == chainFixtureDef.fixture) {
            chainContacts[other] = (chainContacts[other] ?: 0) + 1

            partiallyInside.add(other)
            fullyInside.remove(other)
        }
    }

    override fun endContact(other: Box2DModule, contact: Contact) {
        if (contact.fixtureA == polyFixtureDef.fixture || contact.fixtureB == polyFixtureDef.fixture) {
            polyContact.remove(other)

            partiallyInside.remove(other)
            fullyInside.remove(other)
        } else if (contact.fixtureA == chainFixtureDef.fixture || contact.fixtureB == chainFixtureDef.fixture) {
            chainContacts[other] = (chainContacts[other] ?: 1) - 1
            if (chainContacts[other] == 0) {
                if (polyContact.contains(other)) {
                    partiallyInside.remove(other)
                    fullyInside.add(other)
                } else {
                    partiallyInside.remove(other)
                    fullyInside.remove(other)
                }
            }
        }
    }

    override fun preSolve(other: Box2DModule, contact: Contact, oldManifold: Manifold) {
    }

    override fun postSolve(other: Box2DModule, contact: Contact, impulse: ContactImpulse) {
    }
}