package com.ivieleague.kotlen.physics.box2d

import com.badlogic.gdx.math.Shape2D
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.Fixture
import com.badlogic.gdx.physics.box2d.FixtureDef

/**
 * Created by josep on 1/8/2016.
 */
class FixtureDefPlus : FixtureDef() {
    var shape2D: Shape2D? = null
    var fixture: Fixture? = null
    fun configure(shape2D: Shape2D) {
        this.shape2D = shape2D
    }

    fun make(body: Body) {
        shape = shape2D!!.toBox2D()
        fixture = body.createFixture(this)
        shape.dispose()
    }

    fun unmake() {
        fixture = null
    }
}