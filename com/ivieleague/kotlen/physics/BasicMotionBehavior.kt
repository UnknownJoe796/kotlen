package com.ivieleague.kotlen.physics

import com.badlogic.gdx.math.Vector2
import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.serialize.DirectWrite
import java.util.*

/**
 * Created by josep on 8/15/2015.
 */
class BasicMotionBehavior(
        override val order: Int
) : Behavior, MutableCollection<BasicMotionBehavior.Module> by ArrayList() {

    override fun step(delta: Float) {
        for (module in this) {
            module.position.add(
                    module.velocity.x * delta + module.acceleration.x * .5f * delta * delta,
                    module.velocity.y * delta + module.acceleration.y * .5f * delta * delta)
            module.velocity.add(module.acceleration.x * delta, module.acceleration.y * delta)
        }
    }

    class Module(
            @DirectWrite val position: Vector2 = Vector2(),
            @DirectWrite val velocity: Vector2 = Vector2(),
            @DirectWrite val acceleration: Vector2 = Vector2()
    ) {

        constructor(startup: Module.() -> Unit) : this() {
            startup()
        }

        var x: Float
            get() = position.x
            set(value) {
                position.x = value
            }
        var y: Float
            get() = position.y
            set(value) {
                position.y = value
            }
        var vx: Float
            get() = velocity.x
            set(value) {
                velocity.x = value
            }
        var vy: Float
            get() = velocity.y
            set(value) {
                velocity.y = value
            }
        var ax: Float
            get() = acceleration.x
            set(value) {
                acceleration.x = value
            }
        var ay: Float
            get() = acceleration.y
            set(value) {
                acceleration.y = value
            }

        fun attract(time: Float, other: Module, constant: Float, otherMass: Float, forceCap: Float) {
            val dx = other.x - x
            val dy = other.y - y
            val dist2 = dx * dx + dy * dy
            val dist = Math.sqrt(dist2.toDouble()).toFloat()
            val strength = (constant * otherMass / dist2).coerceAtMost(forceCap)
            vx += time * dx / dist * strength
            vy += time * dy / dist * strength
        }

        fun attract(time: Float, point: Vector2, constant: Float, otherMass: Float, forceCap: Float) {
            val dx = point.x - x
            val dy = point.y - y
            val dist2 = dx * dx + dy * dy
            val dist = Math.sqrt(dist2.toDouble()).toFloat()
            val strength = (constant * otherMass / dist2).coerceAtMost(forceCap)
            vx += time * dx / dist * strength
            vy += time * dy / dist * strength
        }
    }

    override fun start() {
    }

    override fun render() {

    }

    override fun stop() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
    }

}