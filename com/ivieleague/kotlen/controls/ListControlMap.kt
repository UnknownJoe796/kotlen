package com.ivieleague.kotlen.controls

import java.util.*

/**
 * Created by josep on 12/17/2015.
 */
class ListControlMap<T: ControlMap>(
        val max:Int,
        val maker:(Int)->T
) : ControlMap {

    val itemSize: Int = maker(0).slots
    override val slots: Int = itemSize * max
    val map: HashMap<Int, T> = HashMap()
    var startIndex:Int = 0

    lateinit var behavior:ControlBehavior
    override fun setup(behavior: ControlBehavior, startIndex: Int) {
        this.behavior = behavior
        this.startIndex = startIndex
    }

    /**
     * DO NOT CALL THIS FUNCTION UNTIL setup() HAS BEEN CALLED.
     */
    operator fun get(key: Int): T {
        return map[key] ?: {
            val new = maker(key)
            new.setup(behavior, startIndex + key * itemSize)
            map[key] = new
            new
        }()
    }
}