package com.ivieleague.kotlen.controls

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3

/**
 * Created by josep on 12/18/2015.
 */
fun <T> ValueControlMap<T>.bindStep(action:()->T){
    behavior.stepListeners.add{
        val old = get()
        val new = action()
        if(old != new){
            behavior.controlUpdate(slot, new)
        }
    }
}

fun ValueControlMap<Boolean>.bindKey(key:Int){
    behavior.keyDownListeners.add{
        if(it == key){
            behavior.controlUpdate(slot, true)
            true
        } else false
    }
    behavior.keyUpListeners.add{
        if(it == key){
            behavior.controlUpdate(slot, false)
            true
        } else false
    }
}

fun ValueControlMap<Vector2>.bindTouchPosition(){
    behavior.touchDownListeners.add{ screenX: Int, screenY: Int, pointer: Int, button: Int->
        behavior.controlUpdate(slot, Vector2(screenX.toFloat(), screenY.toFloat()))
        true
    }
    behavior.touchDraggedListeners.add{ screenX: Int, screenY: Int, pointer: Int->
        behavior.controlUpdate(slot, Vector2(screenX.toFloat(), screenY.toFloat()))
        true
    }
}

fun ValueControlMap<Boolean>.bindTouchDown(){
    behavior.touchDownListeners.add { screenX: Int, screenY: Int, pointer: Int, button: Int ->
        behavior.controlUpdate(slot, true)
        true
    }
    behavior.touchUpListeners.add { screenX: Int, screenY: Int, pointer: Int, button: Int ->
        behavior.controlUpdate(slot, false)
        true
    }
}