package com.ivieleague.kotlen.controls

import java.util.*

/**
 * Created by josep on 12/17/2015.
 */
abstract class ObjectControlMap: ControlMap {
    override var slots: Int = 0

    val maps:ArrayList<ControlMap> = ArrayList()

    fun <T : ControlMap> add(controlMap: T):T{
        slots += controlMap.slots
        maps.add(controlMap)
        return controlMap
    }

    fun <T> value(initial:T): ValueControlMap<T> = add(ValueControlMap(initial))

    override fun setup(behavior: ControlBehavior, startIndex: Int) {
        var currentSlot = startIndex
        for(map in maps){
            map.setup(behavior, currentSlot)
            currentSlot += map.slots
        }
    }
}