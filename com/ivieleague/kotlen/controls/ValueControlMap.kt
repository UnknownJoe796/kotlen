package com.ivieleague.kotlen.controls

import kotlin.reflect.KProperty

/**
 * Created by josep on 12/17/2015.
 */
class ValueControlMap<T>(val initial:T): ControlMap {
    override val slots: Int
        get() = 1

    var slot:Int = 0

    lateinit var behavior:ControlBehavior
    override fun setup(behavior: ControlBehavior, startIndex: Int) {
        this.behavior = behavior
        slot = startIndex
        behavior.controls[slot] = initial
    }

    @Suppress("UNCHECKED_CAST")
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return behavior.controls[slot] as T
    }

    @Suppress("UNCHECKED_CAST")
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value:T) {
        behavior.controlUpdate(slot, value)
    }

    @Suppress("UNCHECKED_CAST")
    fun get():T{
        return behavior.controls[slot] as T
    }
}