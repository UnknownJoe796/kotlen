package com.ivieleague.kotlen.controls

/**
 * Created by josep on 12/17/2015.
 */
interface ControlMap {
    val slots:Int
    fun setup(behavior:ControlBehavior, startIndex:Int)
}