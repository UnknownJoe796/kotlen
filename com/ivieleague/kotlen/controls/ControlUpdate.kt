package com.ivieleague.kotlen.controls

import com.ivieleague.kotlen.entity.Entity

/**
 * Created by josep on 12/15/2015.
 */
class ControlUpdate(
        val id:Int,
        val value:Any?,
        val time:Double
) {
}