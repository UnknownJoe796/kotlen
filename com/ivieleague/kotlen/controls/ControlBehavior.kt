package com.ivieleague.kotlen.controls

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor
import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.serialize.Serialize
import com.ivieleague.kotlen.serialize.readHashMap
import com.ivieleague.kotlen.serialize.writeHashMap
import java.util.*

/**
 * Created by josep on 12/17/2015.
 */
class ControlBehavior: Behavior, InputProcessor {
    override val order: Int
        get() = Int.MIN_VALUE + 10

    val controls: HashMap<Int, Any?> = HashMap()
    operator fun get(key: Int): Any? = controls[key]
    val controlUpdates: LinkedList<ControlUpdate> = LinkedList()

    var time:Double = 0.0
    var retainControlUpdates: Double = 10.0

    fun controlUpdate(id:Int, value:Any?){
        controlUpdates.addFirst(ControlUpdate(id, value, time))
    }

    fun clearBindings(){
        stepListeners.clear()
        touchDownListeners.clear()
        touchDraggedListeners.clear()
        touchUpListeners.clear()
        keyDownListeners.clear()
        keyTypedListeners.clear()
        keyUpListeners.clear()
        mouseMovedListeners.clear()
        scrolledListeners.clear()
    }

    override fun start() {
        Gdx.input.inputProcessor = this
    }

    val stepListeners: ArrayList<(delta:Float) -> Unit> = ArrayList()
    override fun step(delta: Float) {
        for(listener in stepListeners){
            listener(delta)
        }
        val fromTime = time
        val toTime = time + delta
        for (update in controlUpdates) {
            if (update.time >= toTime) {
                continue
            }
            if (update.time < fromTime) {
                break
            }
            controls[update.id] = update.value
        }
        time = toTime
        removeOldUpdates(toTime)
    }

    override fun render() {

    }

    private fun removeOldUpdates(destTime: Double) {
        var index = 0;
        val earliest = destTime - retainControlUpdates
        for (item in controlUpdates) {
            if (item.time < earliest) {
                break;
            }
            index++
        }
        controlUpdates.subList(index, controlUpdates.size).clear()
    }

    override fun stop() {
        Gdx.input.inputProcessor = null
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
    }


    val touchDownListeners: ArrayList<(screenX: Int, screenY: Int, pointer: Int, button: Int) -> Boolean> = ArrayList()
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        var captured = false
        for(listener in touchDownListeners){
            captured = captured || listener(screenX, screenY, pointer, button)
        }
        return captured
    }

    val touchDraggedListeners: ArrayList<(screenX: Int, screenY: Int, pointer: Int) -> Boolean> = ArrayList()
    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        var captured = false
        for(listener in touchDraggedListeners){
            captured = captured || listener(screenX, screenY, pointer)
        }
        return captured
    }

    val touchUpListeners: ArrayList<(screenX: Int, screenY: Int, pointer: Int, button: Int) -> Boolean> = ArrayList()
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        var captured = false
        for(listener in touchUpListeners){
            captured = captured || listener(screenX, screenY, pointer, button)
        }
        return captured
    }

    val keyDownListeners: ArrayList<(keycode: Int) -> Boolean> = ArrayList()
    override fun keyDown(keycode: Int): Boolean {
        var captured = false
        for(listener in keyDownListeners){
            captured = captured || listener(keycode)
        }
        return captured
    }

    val keyUpListeners: ArrayList<(keycode: Int) -> Boolean> = ArrayList()
    override fun keyUp(keycode: Int): Boolean {
        var captured = false
        for(listener in keyUpListeners){
            captured = captured || listener(keycode)
        }
        return captured
    }

    val keyTypedListeners: ArrayList<(character: Char) -> Boolean> = ArrayList()
    override fun keyTyped(character: Char): Boolean {
        var captured = false
        for(listener in keyTypedListeners){
            captured = captured || listener(character)
        }
        return captured
    }

    val mouseMovedListeners: ArrayList<(screenX: Int, screenY: Int) -> Boolean> = ArrayList()
    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        var captured = false
        for(listener in mouseMovedListeners){
            captured = captured || listener(screenX, screenY)
        }
        return captured
    }

    val scrolledListeners: ArrayList<(amount: Int) -> Boolean> = ArrayList()
    override fun scrolled(amount: Int): Boolean {
        var captured = false
        for(listener in scrolledListeners){
            captured = captured || listener(amount)
        }
        return captured
    }

    companion object{
        init{
            Serialize.objectSerializer<ControlBehavior>({
                writeHashMap(it.controls)
            },{
                readHashMap(it.controls)
                it
            })
        }
    }
}