package com.ivieleague.kotlen.old

import java.util.*

/**
 * Created by josep on 9/2/2015.
 */
open class Bond<T>(init: T) {
    protected var listeners: ArrayList<(v: T) -> Unit> = ArrayList()
    protected var myValue: T = init

    fun get(thisRef: Any?, prop: PropertyMetadata): T {
        return myValue
    }

    fun get(): T {
        return myValue
    }

    fun set(thisRef: Any?, prop: PropertyMetadata, v: T) {
        myValue = v
        update()
    }

    fun set(v: T) {
        myValue = v
        update()
    }

    open fun update() {
        for (listener in listeners) {
            listener(myValue)
        }
    }

    fun bind(body: (v: T) -> Unit) {
        listeners.add(body)
        body(myValue)
    }

    fun unbind(body: (v: T) -> Unit) {
        listeners.remove(body)
    }

    fun clearBindings() {
        listeners.clear()
    }

    override fun toString(): String {
        return "Bond(" + myValue.toString() + ")"
    }
}