package com.ivieleague.kotlen.old

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

/**
 * Created by joseph on 7/25/15.
 */

fun Actor.setCenter(x: Float, y: Float)
{
    setX(x - width * .5f)
    setY(y - height * .5f)
}

fun <T : Actor> Stage.addActor(actor: T, startup: T.() -> Unit): T {
    actor.startup()
    addActor(actor)
    return actor
}

fun <T : Actor> Group.addActor(actor: T, startup: T.() -> Unit): T {
    actor.startup()
    addActor(actor)
    return actor
}

fun Button.onClick(action: () -> Unit) {
    addListener(object : ClickListener() {
        override fun clicked(event: InputEvent?, x: Float, y: Float) {
            super.clicked(event, x, y)
            action()
        }
    })
}