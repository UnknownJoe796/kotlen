package com.ivieleague.kotlen.old

import java.util.*

/**
 * Created by josep on 9/2/2015.
 */
class BondSet() {
    private val list: ArrayList<Bond<*>> = ArrayList()
    fun <T> make(init: T): Bond<T> {
        val newBond = Bond(init)
        list.add(newBond)
        return newBond
    }

    fun clearBindings() {
        for (bond in list) {
            bond.clearBindings()
        }
    }

}