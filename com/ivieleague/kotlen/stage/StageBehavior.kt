package com.ivieleague.kotlen.old

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.viewport.FitViewport
import com.ivieleague.kotlen.Behavior

/**
 * Created by joseph on 7/25/15.
 */
abstract class StageBehavior(virtualWidth: Float, virtualHeight: Float, order: Int = 0) : Behavior
{
    override val order: Int = order

    var stageWidth: Float = virtualWidth
    var stageHeight: Float = virtualHeight
    var hundreths: Float = Math.min(virtualWidth, virtualHeight) / 100f

    var stage: Stage? = null
    var batch: SpriteBatch? = null
    var skin: Skin? = null

    var color: Color = Color.LIGHT_GRAY

    override fun start() {
        stage = Stage(FitViewport(stageWidth, stageHeight))
        skin = Skin(Gdx.files.internal("uiskin.json"))
        batch = SpriteBatch()
        Gdx.input.inputProcessor = stage;
        createStage(stage!!, skin!!)
    }

    protected abstract fun createStage(stage: Stage, skin: Skin);

    override fun resize(width: Int, height: Int)
    {
        stage?.getViewport()?.update(width, height, true)
    }

    override fun step(delta: Float)
    {
    }

    override fun render() {
        if (color.a > .5f)
        {
            Gdx.gl.glClearColor(color.r, color.g, color.b, 1f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        }
        batch?.begin();
        stage?.draw();
        batch?.end();
    }

    override fun stop()
    {
        stage?.dispose()
        skin?.dispose()
        batch?.dispose()
    }
}