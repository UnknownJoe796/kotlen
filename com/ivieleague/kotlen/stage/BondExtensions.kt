package com.ivieleague.kotlen.old

import com.badlogic.gdx.scenes.scene2d.ui.TextArea
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener

/**
 * Created by josep on 9/2/2015.
 */

fun TextArea.bindString(bond: Bond<String>) {
    this.addListener {
        if (it is ChangeListener.ChangeEvent) {
            if (bond.get() != getText()) bond.set(getText())
            true
        } else false
    }
    bond.bind {
        if (it != getText()) {
            setText(it)
        }
    }
}