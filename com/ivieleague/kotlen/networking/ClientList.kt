package com.ivieleague.kotlen.networking

import com.ivieleague.kotlen.serialize.Serialize
import java.util.*

/**
 * Created by josep on 11/9/2015.
 */

class ClientList() : ArrayList<String>() {
    companion object {
        init {
            Serialize.atomicSerializer(
                    {
                        writeShort(it.size)
                        for (item in it) {
                            writeUTF(item)
                        }
                    }, {
                val it = ClientList()
                for (i in 1..readShort()) {
                    it.add(readUTF())
                }
                it
            }
            )
        }
    }
}