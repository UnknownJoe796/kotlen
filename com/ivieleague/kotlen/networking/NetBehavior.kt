package com.ivieleague.kotlen.networking

import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.serialize.toByteArray
import com.ivieleague.kotlen.simulation.Simulation
import java.util.*

/**
 * Created by josep on 12/11/2015.
 */
class NetBehavior(val simulation: Simulation) : Behavior {

    override val order: Int = Int.MIN_VALUE;

    public var frequency: Double = .05;
    public var max: Int = 60;

    private var server: Server? = null
    private var client: Client? = null

    private val states: LinkedList<Packet> = LinkedList()
    private var timeSinceLast: Double = 0.0;

    override fun start() {
    }

    override fun step(delta: Float) {
        stateRecordStep(delta)
        if (server != null) {
            serverStep(delta)
        } else {
            clientStep(delta)
        }
    }

    override fun render() {
    }

    override fun stop() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
    }

    fun stateRecordStep(delta: Float) {
        //Record a game state
        timeSinceLast += delta
        if (timeSinceLast > frequency) {
            timeSinceLast -= frequency
            states.push(Packet(simulation.time, toByteArray { stream ->
                simulation.serialize(stream)
            }))
        }
    }

    fun serverStep(delta: Float) {
        //send old state
        states.last
    }

    fun clientStep(delta: Float) {

    }

    /**
     * BOTH
     * Retains a list of previous states going back about five seconds
     *
     * SERVER
     * Incorporates control events
     * Sends full states to clients
     *
     * CLIENT
     * Incorporates states from server
     * Sends personal control events
     */
}