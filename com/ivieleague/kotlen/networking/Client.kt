package com.ivieleague.kotlen.networking

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Net
import com.badlogic.gdx.net.SocketHints
import com.ivieleague.kotlen.serialize.SerializeSocket
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Created by josep on 11/9/2015.
 */
class Client(val serverAddress: String, val port: Int) {
    val socketHints: SocketHints = SocketHints().apply {

    }
    val socket: SerializeSocket = SerializeSocket(Gdx.net.newClientSocket(Net.Protocol.TCP, serverAddress, port, socketHints))
    val output: ConcurrentLinkedQueue<Any> get() = socket.output

    var clientList: ClientList? = null

    var onClose: () -> Unit
        get() = socket.onClosed
        set(value) {
            socket.onClosed = value
        }

    fun send(obj: Any) {
        socket.send(obj)
    }

    fun close() {
        socket.close()
    }

    init {
        socket.onReceive = {
            println("received: $it")
            if (it is ClientList) {
                clientList = it
            }
            it is ClientList
        }
    }
}