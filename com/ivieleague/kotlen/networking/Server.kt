package com.ivieleague.kotlen.networking

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Net
import com.badlogic.gdx.net.ServerSocket
import com.badlogic.gdx.net.ServerSocketHints
import com.badlogic.gdx.net.SocketHints
import com.badlogic.gdx.utils.GdxRuntimeException
import com.ivieleague.kotlen.serialize.SerializeSocket
import java.net.SocketTimeoutException
import java.util.*

/**
 * Created by josep on 11/7/2015.
 */
class Server(val myAddress: String, val port: Int) {
    var canJoin: Boolean = true

    val clients: HashMap<String, Client> = HashMap()
    val sockets = clients.values.map { it.socket }
    val output = sockets.map { it.output }

    val serverSocketHints: ServerSocketHints = ServerSocketHints().apply {
        acceptTimeout = 4000
    }
    val socketHints: SocketHints = SocketHints().apply {

    }
    val socket: ServerSocket = Gdx.net.newServerSocket(Net.Protocol.TCP, port, serverSocketHints)

    var onJoin: (client: Client) -> Unit = {}
    var onLeave: (client: Client) -> Unit = {}
    var onClose: () -> Unit = {}

    private var shouldClose: Boolean = false
    val isClosed: Boolean get() = shouldClose

    val thread: Thread = Thread(object : Runnable {
        override fun run() {
            while (!shouldClose) {
                try {
                    val acceptedSocket = socket.accept(socketHints)

                    val newSocket = SerializeSocket(acceptedSocket)
                    newSocket.onClosed = {
                        val client = clients.remove(newSocket.remoteAddress)
                        if (client != null) {
                            onLeave(client)
                        }
                        send(makeClientList())
                    }

                    val newClient = Client(newSocket.remoteAddress, newSocket)
                    clients.put(newSocket.remoteAddress, newClient)

                    send(makeClientList())

                    onJoin(newClient)

                } catch(e: GdxRuntimeException) {
                    if (e.cause is SocketTimeoutException) {
                        //println("timeout")
                    } else {
                        e.printStackTrace()
                    }
                } catch(e: Exception) {
                    e.printStackTrace()
                }
            }
            onClose()
        }
    })

    private fun makeClientList(): ClientList {
        val list = ClientList()
        list.add(myAddress)
        list.addAll(clients.keys)
        return list
    }

    init {
        thread.start()
    }

    @Synchronized fun send(obj: Any) {
        for ((address, client) in clients) {
            client.socket.send(obj)
        }
    }

    fun close() {
        shouldClose = true
        thread.join(5000)
    }

    class Client(
            var address: String,
            var socket: SerializeSocket
    ) {
    }


}
