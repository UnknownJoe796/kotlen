package com.ivieleague.kotlen.networking

import com.ivieleague.kotlen.serialize.Serialize
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream

/**
 * Created by josep on 11/5/2015.
 */
class Packet(val time: Double, val byteArray: ByteArray) {

    constructor(time: Double, action: DataOutputStream.() -> Unit) : this(time, {
        val stream = ByteArrayOutputStream()
        val dataStream = DataOutputStream(stream)
        dataStream.action()
        stream.toByteArray()
    }())

    companion object {
        init {
            Serialize.atomicSerializer({
                writeDouble(it.time)
                writeInt(it.byteArray.size)
                write(it.byteArray)
            }, {
                val time = readDouble()
                val array = ByteArray(readInt())
                read(array)
                Packet(time, array)
            })
        }
    }
}