package com.ivieleague.kotlen

import com.badlogic.gdx.utils.Disposable

/**
 * Created by josep on 9/5/2015.
 */
interface Behavior : Comparable<Behavior>, Disposable {
    val order: Int
    override fun compareTo(other: Behavior): Int = order.compareTo(other.order)

    fun start()
    fun step(delta: Float)
    fun render()
    fun stop()

    fun resize(width: Int, height: Int)
    fun onBackPressed(action: () -> Unit) {
        action()
    }
}