package com.ivieleague.kotlen.simulation

import com.ivieleague.kotlen.BehaviorList
import com.ivieleague.kotlen.controls.ControlBehavior
import com.ivieleague.kotlen.entity.EntityBehavior
import com.ivieleague.kotlen.serialize.InstanceSerializable
import com.ivieleague.kotlen.serialize.readTyped
import com.ivieleague.kotlen.serialize.writeTyped
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * Created by josep on 9/7/2015.
 */
abstract class Simulation() : BehaviorList(0), InstanceSerializable {

    abstract val entities: EntityBehavior<out Simulation>
    val controlBehavior: ControlBehavior = ControlBehavior()

    var time: Double = 0.0

    override fun start() {
        controlBehavior.start()
        entities.start()
        super.start()
    }

    fun stepTo(destTime: Double, stepTime: Float = 0.05f) {
        while (time + stepTime < destTime) {
            controlBehavior.step(stepTime)
            entities.step(stepTime)
            super.step(stepTime)
            time += stepTime
        }
        if(destTime - time > .01){
            val smallStep = (destTime - time).toFloat()
            controlBehavior.step(smallStep)
            entities.step(smallStep)
            super.step(smallStep)
        }
        time = destTime
    }

    override fun step(delta: Float) {
        stepTo(time + delta, delta)
    }

    override fun stop() {
        super.stop()
        entities.stop()
        controlBehavior.stop()
    }

    override fun dispose() {
        super.dispose()
        entities.dispose()
        controlBehavior.dispose()
    }

    override fun serialize(stream: DataOutputStream) {
        stream.writeDouble(time)
        stream.writeTyped(entities)
        stream.writeTyped(controlBehavior)
    }

    override fun deserialize(stream: DataInputStream) {
        time = stream.readDouble()
        stream.readTyped(entities)
        stream.readTyped(controlBehavior)
    }

    override fun onBackPressed(action: () -> Unit) {
        action()
    }

    /*fun deserializeMergeControlUpdates(stream: DataInputStream) {
        time = stream.readDouble()
        prevTime = time
        stream.readTyped(entities)
        stream.readHashMap(players)
        val newUpdates = stream.readLinkedList<Player.Event>()
        mergeControlUpdates(newUpdates)
    }

    //TODO: Maybe unit test
    private fun mergeControlUpdates(other: LinkedList<Player.Event>) {
        val iter = controlUpdates.listIterator()
        val otherIter = other.iterator()
        if (!otherIter.hasNext()) return
        var otherItem = otherIter.next()
        while (iter.hasNext()) {
            val item = iter.next()
            if (item == otherItem) {
                if (!otherIter.hasNext()) return;
                otherItem = otherIter.next()
            }
            if (otherItem.time > item.time) {
                iter.previous()
                iter.add(otherItem)
                iter.next()
                iter.next()
                if (!otherIter.hasNext()) return;
                otherItem = otherIter.next()
            }
        }
    }*/
}