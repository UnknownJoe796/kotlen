package com.ivieleague.kotlen

import com.ivieleague.kotlen.collections.orderedAdd
import java.util.*

/**
 * Created by josep on 9/5/2015.
 */
open class BehaviorList(
        override val order: Int = 0,
        private val behaviors: LinkedList<Behavior> = LinkedList()
) : Behavior, List<Behavior> by behaviors {

    private val behaviorAdditionOrder: ArrayList<Behavior> = ArrayList()
    private var isRunning: Boolean = false

    val map: HashMap<String, Behavior> = HashMap()

    /**
     * This function cannot be used while the list is iterating.
     */
    fun <T : Behavior> add(behavior: T): T {
        map[behavior.javaClass.name] = behavior
        behaviorAdditionOrder.add(behavior)
        if (isRunning) {
            behavior.start()
        }
        return behavior
    }

    /**
     * This function cannot be used while the list is iterating.
     */
    inline fun <reified T: Behavior> request(identifier:String, maker:()->T): T{
        val current = map[identifier] as? T
        if(current != null) return current
        else{
            val new = maker()
            map[identifier] = new
            return new
        }
    }

    /**
     * This function cannot be used while the list is iterating.
     */
    inline fun <reified T: Behavior> request(maker:()->T):T = request(T::class.java.name, maker)

    override fun start() {
        for (behavior in behaviorAdditionOrder) {
            behaviors.orderedAdd(behavior)
            behavior.start()
        }
        isRunning = true
    }

    override fun step(delta: Float) {
        behaviors.forEach { it.step(delta) }
    }

    override fun render() {
        behaviors.forEach { it.render() }
    }

    override fun stop() {
        isRunning = false
        for (behaviorIndex in behaviorAdditionOrder.size - 1 downTo 0) {
            val behavior = behaviorAdditionOrder[behaviorIndex]
            behavior.stop()
        }
    }

    override fun resize(width: Int, height: Int) {
        behaviors.forEach { it.resize(width, height) }
    }

    override fun dispose() {
        for (behaviorIndex in behaviorAdditionOrder.size - 1 downTo 0) {
            val behavior = behaviorAdditionOrder[behaviorIndex]
            behavior.dispose()
        }
        behaviors.clear()
        map.clear()
    }
}