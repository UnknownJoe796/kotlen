package com.ivieleague.kotlen.language

import com.badlogic.gdx.math.*
import java.util.*

/**
 * Created by joseph on 7/17/15.
 */
/**
 * Direction of angle is the same as the direciton of Math.
 */
fun Vector2_polar(distance: Float, radians: Double): Vector2 {
    return Vector2(Math.cos(radians).toFloat() * distance, Math.sin(radians).toFloat() * distance)
}

fun Vector2_polarDegrees(distance: Float, degrees: Float): Vector2 {
    return Vector2(Math.cos(degrees * 180 / Math.PI).toFloat() * distance, Math.sin(degrees * 180 / Math.PI).toFloat() * distance)
}

fun Vector2.setPolar(distance: Float, radians: Double): Vector2 {
    x = Math.cos(radians).toFloat() * distance
    y = Math.sin(radians).toFloat() * distance
    return this
}

fun Vector2.addPolar(distance: Float, radians: Double): Vector2 {
    x += Math.cos(radians).toFloat() * distance
    y += Math.sin(radians).toFloat() * distance
    return this
}

fun Vector3.toVector2(): Vector2 {
    return Vector2(x, y)
}

fun Vector2.toVector3(): Vector3 {
    return Vector3(x, y, 0f)
}

val Rectangle.centerX: Float get() = x + width / 2
val Rectangle.centerY: Float get() = y + height / 2
val Rectangle.xRange: ClosedRange<Float> get() = x..x + width
val Rectangle.yRange: ClosedRange<Float> get() = y..y + height


fun Shape2D.cpy(): Shape2D {
    return when (this) {
        is Polygon -> cpy()
        is Circle -> cpy()
        is Polyline -> cpy()
        is Ellipse -> cpy()
        is Rectangle -> cpy()
        else -> throw IllegalArgumentException()
    }
}

fun Polygon.cpy(): Polygon {
    return Polygon(vertices).apply {
        rotation = this@cpy.rotation
        setScale(this@cpy.scaleX, this@cpy.scaleY)
        translate(this@cpy.x, this@cpy.y)
    }
}

fun Circle.cpy(): Circle {
    return Circle(this)
}

fun Polyline.cpy(): Polyline {
    return Polyline(vertices).apply {
        rotation = this@cpy.rotation
        setScale(this@cpy.scaleX, this@cpy.scaleY)
        translate(this@cpy.x, this@cpy.y)
    }
}

fun Ellipse.cpy(): Ellipse {
    return Ellipse(this)
}

fun Rectangle.cpy(): Rectangle {
    return Rectangle(this)
}


fun Polygon.border(): Polyline {
    return Polyline(vertices).apply {
        rotation = this@border.rotation
        setScale(this@border.scaleX, this@border.scaleY)
        translate(this@border.x, this@border.y)
    }
}


fun Shape2D.centerAndGetOffset(): Vector2 {
    return when (this) {
        is Polygon -> centerAndGetOffset()
        is Circle -> centerAndGetOffset()
        is Polyline -> centerAndGetOffset()
        is Ellipse -> centerAndGetOffset()
        is Rectangle -> centerAndGetOffset()
        else -> throw IllegalArgumentException()
    }
}

fun Polygon.centerAndGetOffset(): Vector2 {
    val result = Vector2(x, y)
    setPosition(0f, 0f)
    return result
}

fun Circle.centerAndGetOffset(): Vector2 {
    val result = Vector2(x, y)
    setPosition(0f, 0f)
    return result
}

fun Polyline.centerAndGetOffset(): Vector2 {
    val result = Vector2(x, y)
    setPosition(0f, 0f)
    return result
}

fun Ellipse.centerAndGetOffset(): Vector2 {
    val result = Vector2(x, y)
    setPosition(0f, 0f)
    return result
}

fun Rectangle.centerAndGetOffset(): Vector2 {
    val hx = width / 2
    val hy = height / 2
    val result = Vector2(x + hx, y + hy)
    setPosition(-hx, -hy)
    return result
}


fun Polygon.unrotateAndGetAngle(): Float {
    val result = rotation
    rotation = 0f
    return result
}


val Shape2D.position: Vector2 get() {
    return when (this) {
        is Polygon -> position
        is Circle -> position
        is Polyline -> position
        is Ellipse -> position
        is Rectangle -> position
        else -> throw IllegalArgumentException()
    }
}

val Polygon.position: Vector2 get() {
    return Vector2(x, y)
}

val Circle.position: Vector2 get() {
    return Vector2(x, y)
}

val Polyline.position: Vector2 get() {
    return Vector2(x, y)
}

val Ellipse.position: Vector2 get() {
    return Vector2(x, y)
}

val Rectangle.position: Vector2 get() {
    return Vector2(centerX, centerY)
}


fun Shape2D.toPolygon(): Polygon {
    return when (this) {
        is Polygon -> this
        is Circle -> toPolygon(12)
        is Polyline -> toPolygon()
        is Ellipse -> toPolygon(12)
        is Rectangle -> toPolygon()
        else -> throw IllegalArgumentException()
    }
}

fun Circle.toPolygon(sides: Int = 12): Polygon {
    val array = FloatArray(sides * 2)
    repeat(sides) {
        val radians = it / sides * Math.PI * 2
        array[it * 2] = x + Math.cos(radians).toFloat() * radius
        array[it * 2 + 1] = y - Math.sin(radians).toFloat() * radius
    }
    return Polygon(array)
}

fun Polyline.toPolygon(): Polygon {
    return Polygon(vertices).apply {
        rotation = this@toPolygon.rotation
        setScale(this@toPolygon.scaleX, this@toPolygon.scaleY)
        translate(this@toPolygon.x, this@toPolygon.y)
    }
}

fun Ellipse.toPolygon(sides: Int = 12): Polygon {
    val array = FloatArray(sides * 2)
    repeat(sides) {
        val radians = it / sides * Math.PI * 2
        array[it * 2] = x + Math.cos(radians).toFloat() * width
        array[it * 2 + 1] = y - Math.sin(radians).toFloat() * height
    }
    return Polygon(array)
}

fun Rectangle.toPolygon(): Polygon {
    val poly = Polygon(floatArrayOf(
            0f, 0f,
            width, 0f,
            width, height,
            0f, height))
    poly.setPosition(x, y)
    return poly
}


fun Polygon.vertexIterator(temp: Vector2 = Vector2()): Iterator<Vector2> = object : Iterator<Vector2> {

    val vertices = this@vertexIterator.vertices
    var index: Int = 0

    override fun hasNext(): Boolean = index + 3 < vertices.size

    override fun next(): Vector2 {
        temp.x = vertices[index]
        temp.y = vertices[index + 1]
        index += 2
        return temp
    }
}

fun Polygon.lineIterator(): Iterator<Pair<Vector2, Vector2>> = object : Iterator<Pair<Vector2, Vector2>> {

    val tempA = Vector2()
    val tempB = Vector2()
    val pair = tempA to tempB

    val vertices = this@lineIterator.vertices
    var index: Int = 0

    override fun hasNext(): Boolean = index + 1 < vertices.size

    override fun next(): Pair<Vector2, Vector2> {
        tempA.x = vertices[index]
        tempA.y = vertices[index + 1]
        if (index + 2 >= vertices.size) {
            tempB.x = vertices[0]
            tempB.y = vertices[1]
        } else {
            tempB.x = vertices[index + 2]
            tempB.y = vertices[index + 3]
        }
        index += 2
        return pair
    }
}

fun Polygon.splitConvex(pointA: Vector2, pointB: Vector2): Array<Polygon>? {

    val pointATransformed: Vector2 = pointA.cpy().sub(x, y).scl(1 / scaleX, 1 / scaleY).rotate(-rotation)
    val pointBTransformed: Vector2 = pointB.cpy().sub(x, y).scl(1 / scaleX, 1 / scaleY).rotate(-rotation)

    val vertices = vertices

    val intersections = ArrayList<Pair<Int, Vector2>>()
    val temp: Vector2 = Vector2()
    var lineIndex: Int = 0
    for ((first, second) in lineIterator()) {
        if (Intersector.intersectSegments(first, second, pointATransformed, pointBTransformed, temp)) {
            intersections.add(lineIndex to temp.cpy())
        }
        lineIndex++
    }
    if (intersections.size != 2) return null

    val innerList = ArrayList<Float>()
    innerList.add(intersections[0].second.x)
    innerList.add(intersections[0].second.y)
    for (v in vertices.sliceArray((intersections[0].first + 1) * 2..intersections[1].first * 2 + 1)) {
        innerList.add(v)
    }
    innerList.add(intersections[1].second.x)
    innerList.add(intersections[1].second.y)

    val outerList = ArrayList<Float>()
    outerList.add(intersections[1].second.x)
    outerList.add(intersections[1].second.y)
    for (v in vertices.sliceArray((intersections[1].first + 1) * 2..vertices.size - 1)) {
        outerList.add(v)
    }
    for (v in vertices.sliceArray(0..intersections[0].first * 2 + 1)) {
        outerList.add(v)
    }
    outerList.add(intersections[0].second.x)
    outerList.add(intersections[0].second.y)

    val polyA = Polygon(innerList.toFloatArray())
    polyA.rotation = rotation
    polyA.setScale(scaleX, scaleY)
    polyA.setPosition(x, y)

    val polyB = Polygon(outerList.toFloatArray())
    polyB.rotation = rotation
    polyB.setScale(scaleX, scaleY)
    polyB.setPosition(x, y)

    return arrayOf(
            polyA,
            polyB
    )
}