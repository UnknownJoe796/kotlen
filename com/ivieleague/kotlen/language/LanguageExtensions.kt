package com.ivieleague.kotlen.language

import java.util.concurrent.*

/**
 * Created by joseph on 7/17/15.
 */

inline fun <T> forWith(collection: Collection<T>, func: T.() -> Unit) {
    for (item in collection) {
        item.func()
    }
}

inline fun <T> forWithIndex(collection: Collection<T>, func: T.(index: Int) -> Unit) {
    var index = 0
    for (item in collection) {
        item.func(index)
        index++
    }
}

private val executor: ExecutorService = Executors.newCachedThreadPool()
fun <T> runTimeout(milliseconds: Long, task: () -> T): T? {
    val future = executor.submit(task)
    try {
        return future.get(milliseconds, TimeUnit.MILLISECONDS)
    } catch (ex: TimeoutException) {
        return null
    } catch (e: InterruptedException) {
        return null
    } catch (e: ExecutionException) {
        return null
    } finally {
        future.cancel(false); // may or may not desire this
    }
}

inline fun Double.radToDeg() = Math.toDegrees(this)
inline fun Double.degToRad() = Math.toRadians(this)
inline fun Float.radToDeg() = Math.toDegrees(this.toDouble()).toFloat()
inline fun Float.degToRad() = Math.toRadians(this.toDouble()).toFloat()