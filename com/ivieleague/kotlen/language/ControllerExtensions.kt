package com.ivieleague.kotlen.language

import com.badlogic.gdx.controllers.Controller
import com.badlogic.gdx.controllers.Controllers
import com.badlogic.gdx.controllers.PovDirection
import com.badlogic.gdx.math.Vector2

/**
 * Created by joseph on 7/17/15.
 */

fun Controllers_getController(index: Int): Controller?
{
    val list = Controllers.getControllers()
    if (list.size > index)
        return list[index]
    else
        return null
}

fun Controller.getDoubleAxis(x: Int, y: Int): Vector2
{
    return Vector2(getAxis(x), getAxis(y))
}

class XBox360Pad(controller: Controller, yDown: Boolean)
{
    private val controller = controller;
    private val yFactor = if (yDown) 1 else -1

    var deadzone: Float = .1f

    val left: Vector2 get()
    {
        val value = Vector2(controller.getAxis(AXIS_LEFT_X), controller.getAxis(AXIS_LEFT_Y) * yFactor)
        if (value.len2() < deadzone) return Vector2.Zero
        else return value
    }
    val right: Vector2 get()
    {
        val value = Vector2(controller.getAxis(AXIS_RIGHT_X), controller.getAxis(AXIS_RIGHT_Y) * yFactor)
        if (value.len2() < deadzone) return Vector2.Zero
        else return value
    }
    val a: Boolean get() = controller.getButton(BUTTON_A)
    val b: Boolean get() = controller.getButton(BUTTON_B)
    val x: Boolean get() = controller.getButton(BUTTON_X)
    val y: Boolean get() = controller.getButton(BUTTON_Y)
    val back: Boolean get() = controller.getButton(BUTTON_BACK)
    val start: Boolean get() = controller.getButton(BUTTON_START)
    val dpad: PovDirection get() = controller.getPov(0)
    val lt: Float get() = controller.getAxis(AXIS_LEFT_TRIGGER)
    val rt: Float get() = controller.getAxis(AXIS_RIGHT_TRIGGER)
    val lb: Boolean get() = controller.getButton(BUTTON_LB)
    val rb: Boolean get() = controller.getButton(BUTTON_RB)

    companion object
    {
        fun isXboxController(controller: Controller): Boolean
        {
            return controller.getName().toLowerCase().contains("xbox")
        }

        fun exists(index: Int): Boolean {
            return Controllers.getControllers().size > index
        }

        fun get(index: Int, yDown: Boolean = false): XBox360Pad
        {
            val list = Controllers.getControllers()
            if (list.size > index)
                return XBox360Pad(list[index], yDown)
            else
                throw IllegalArgumentException("Controller is not in existance!")
        }

        fun opt(index: Int, yDown: Boolean = false): XBox360Pad?
        {
            val list = Controllers.getControllers()
            if (list.size > index)
                return XBox360Pad(list[index], yDown)
            else
                return null
        }

        val BUTTON_X: Int = 2
        val BUTTON_Y: Int = 3
        val BUTTON_A: Int = 0
        val BUTTON_B: Int = 1
        val BUTTON_BACK: Int = 6
        val BUTTON_START: Int = 7
        val BUTTON_LB: Int = 4
        val BUTTON_L3: Int = 8
        val BUTTON_RB: Int = 5
        val BUTTON_R3: Int = 9
        val AXIS_LEFT_X: Int = 0
        val AXIS_LEFT_Y: Int = 1
        val AXIS_LEFT_TRIGGER: Int = 4
        val AXIS_RIGHT_X: Int = 2
        val AXIS_RIGHT_Y: Int = 3
        val AXIS_RIGHT_TRIGGER: Int = 5
    }
}