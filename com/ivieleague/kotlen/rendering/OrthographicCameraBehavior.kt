package com.ivieleague.kotlen.rendering

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.language.centerX
import com.ivieleague.kotlen.language.centerY

/**
 * Created by josep on 8/15/2015.
 */
class OrthographicCameraBehavior(val backgroundColor: Color = Color.BLACK, val yDown: Boolean = true) : OrthographicCamera(), Behavior {

    override val order: Int get() = Int.MIN_VALUE

    constructor(viewport: Vector2, backgroundColor: Color = Color.BLACK, yDown: Boolean = true) : this(backgroundColor, yDown) {
        this.viewport.set(viewport)
        position.set(viewport.x / 2, viewport.y / 2, 0f)
    }

    val screenSize: Vector2 = Vector2()
    val screenRatio: Float get() = screenSize.x / screenSize.y

    var keepRatio: Boolean = true

    /**
     * How many world units to show on the screen.
     */
    val viewport: Vector2 = Vector2()

    /**
     * How many world units to show in the minimum dimension.
     */
    var scale: Float
        get() {
            if (screenSize.y > screenSize.x) {
                return viewport.x
            } else {
                return viewport.y
            }
        }
        set(value) {
            viewport.x = value
            viewport.y = value
        }

    fun showRect(rect: Rectangle) {
        viewport.set(rect.width, rect.height)
        position.set(rect.centerX, rect.centerY, 0f);
    }

    override fun resize(width: Int, height: Int) {
        screenSize.set(width.toFloat(), height.toFloat())
    }

    override fun start() {
    }

    override fun step(delta: Float) {
    }

    val storedPosition = Vector3()
    override fun render() {
        storedPosition.set(position)
        if (keepRatio) {
            if (screenSize.y / viewport.y > screenSize.x / viewport.x)
                setToOrtho(yDown, viewport.x, viewport.x * screenSize.y / screenSize.x)
            else
                setToOrtho(yDown, viewport.y * screenSize.x / screenSize.y, viewport.y)
        } else {
            setToOrtho(yDown, viewport.x, viewport.y)
        }
        position.set(storedPosition)
        update()
        Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glEnable(GL20.GL_BLEND)
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    override fun stop() {
    }

    override fun dispose() {
    }

}