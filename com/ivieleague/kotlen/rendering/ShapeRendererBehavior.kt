package com.ivieleague.kotlen.rendering

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by joseph on 6/30/15.
 */
class ShapeRendererBehavior(
        override val order: Int = Int.MAX_VALUE / 2,
        val camera: Camera? = null,
        val solid: Boolean = true
) : Behavior, MutableList<ShapeRenderer.() -> Unit> by ArrayList() {

    var renderer: ShapeRenderer? = null

    override fun start() {
        renderer = ShapeRenderer()
    }

    override fun step(delta: Float) {
    }

    override fun render() {
        if (renderer != null) {
            val r = renderer!!
            if (camera != null) {
                r.projectionMatrix = camera.combined
            }
            r.color = Color.WHITE
            r.begin(if (solid) ShapeRenderer.ShapeType.Filled else ShapeRenderer.ShapeType.Line)
            for (drawAction in this) {
                r.drawAction()
            }
            r.end()
        }
    }

    override fun stop() {
        renderer?.dispose()
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
    }
}