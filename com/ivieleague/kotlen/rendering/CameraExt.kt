package com.ivieleague.kotlen.rendering

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.ivieleague.kotlen.language.toVector2
import com.ivieleague.kotlen.language.toVector3

/**
 * Created by josep on 1/2/2016.
 */


fun Camera?.unproject(screenPoint: Vector2): Vector2 {
    return this?.unproject(screenPoint.toVector3())?.toVector2() ?: screenPoint
}

fun Camera?.unproject(screenX: Int, screenY: Int): Vector2 {
    return this?.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0f))?.toVector2() ?: Vector2(screenX.toFloat(), screenY.toFloat())
}

fun Camera?.project(worldPoint: Vector2): Vector2 {
    return this?.project(worldPoint.toVector3())?.toVector2() ?: worldPoint
}