package com.ivieleague.kotlen.rendering

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.ivieleague.kotlen.Behavior
import java.util.*

/**
 * Created by joseph on 6/30/15.
 */
class SpriteBatchBehavior(
        override val order: Int = Int.MAX_VALUE / 2,
        val camera: Camera? = null
) : Behavior, MutableList<SpriteBatch.() -> Unit> by ArrayList() {

    var renderer: SpriteBatch? = null

    override fun resize(width: Int, height: Int) {
    }

    override fun start() {
        renderer = SpriteBatch()
    }

    override fun step(delta: Float) {
    }

    override fun render() {
        if (renderer != null) {
            val r = renderer!!
            if (camera != null) {
                r.projectionMatrix = camera.combined
            }
            r.begin()
            for (drawAction in this) {
                r.drawAction()
            }
            r.end()
        }
    }

    override fun stop() {
        renderer?.dispose()
    }

    override fun dispose() {
    }
}