package com.ivieleague.kotlen.rendering

import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2

/**
 * Created by josep on 12/19/2015.
 */

fun SpriteBatch.text(str:String, font:BitmapFont, height:Float, position: Vector2)
        = text(str, font, height, position.x, position.y)
fun SpriteBatch.text(str:String, font:BitmapFont, height:Float, x:Float = 0f, y:Float = 0f){
    val layout = GlyphLayout(font, str)
    val original = transformMatrix.cpy()
    val new = transformMatrix.cpy()
    new.translate(x, y, 0f)
    new.scale(height, height, height)
    transformMatrix = new
    font.draw(this, str, layout.width * -.5f, layout.height * -.5f)
    transformMatrix = original
}

fun ShapeRenderer.polygonFilledConvex(vertices: List<Vector2>) {
    if (vertices.size >= 3) {
        repeat(vertices.size - 2) {
            triangle(vertices[0].x, vertices[0].y, vertices[it + 1].x, vertices[it + 1].y, vertices[it + 2].x, vertices[it + 2].y)
        }
    }
}

fun ShapeRenderer.polygonFilledConvex(size: Int, getVertexFun: (Int, Vector2) -> Unit) {
    if (size >= 3) {
        val vertexA = Vector2()
        getVertexFun(0, vertexA)
        val vertexB = Vector2()
        val vertexC = Vector2()
        repeat(size - 2) {
            getVertexFun(it + 1, vertexB)
            getVertexFun(it + 2, vertexC)
            triangle(vertexA.x, vertexA.y, vertexB.x, vertexB.y, vertexC.x, vertexC.y)
        }
    }
}

fun ShapeRenderer.polygonFilledConvex(vertices: FloatArray) {
    if (vertices.size / 2 >= 3) {
        repeat(vertices.size / 2 - 2) {
            triangle(
                    vertices[0],
                    vertices[1],
                    vertices[it * 2 + 2],
                    vertices[it * 2 + 3],
                    vertices[it * 2 + 4],
                    vertices[it * 2 + 5]
            )
        }
    }
}