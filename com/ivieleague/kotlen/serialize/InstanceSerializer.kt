package com.ivieleague.kotlen.serialize

import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * Created by josep on 10/31/2015.
 */

interface InstanceSerializable {
    fun serialize(stream: DataOutputStream)
    fun deserialize(stream: DataInputStream)
}

class InstanceSerializer<T : Any>(type: Class<T>) : ISerializer<T> {
    override val type: Class<T> = type

    override fun write(stream: DataOutputStream, item: T) {
        (item as InstanceSerializable).serialize(stream)
    }

    override fun read(stream: DataInputStream): T {
        val item = type.newInstance()
        (item as InstanceSerializable).deserialize(stream)
        return item
    }

    override fun read(stream: DataInputStream, item: T): T {
        (item as InstanceSerializable).deserialize(stream)
        return item
    }

}