package com.ivieleague.kotlen.serialize

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.util.*

/**
 * Serializes just about anything.  Very customizable.
 * Created by josep on 9/5/2015.
 */

object Serialize {
    private val NULL: Byte = 0x00
    private val NOT_NULL: Byte = 0x01
    private val CLASS_REGISTERED: Byte = 0x02

    val serializers: HashMap<Class<*>, ISerializer<*>> = HashMap()

    private val quickCodes: HashMap<Short, Class<*>> = HashMap()
    private val quickCodesReverse: HashMap<Class<*>, Short> = HashMap()

    private val notCodedSet: HashSet<String> = HashSet()

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> getSerializer(type: Class<T>): ISerializer<T> {
        return serializers[type] as? ISerializer<T> ?:
                if (!InstanceSerializable::class.java.isAssignableFrom(type))
                    addSerializer(FieldSerializer(type))
                else
                    addSerializer(InstanceSerializer(type))
    }

    fun getSerializerAny(type: Class<*>): ISerializer<*> {
        return serializers[type] ?:
                if (!InstanceSerializable::class.java.isAssignableFrom(type))
                    addSerializer(FieldSerializer(type))
                else
                    addSerializer(InstanceSerializer(type))
    }

    //Reading and writing

    fun writeType(stream: DataOutputStream, type: Class<*>?) {
        if (type == null) {
            stream.writeByte(NULL.toInt())
        } else {
            val quickCode = quickCodesReverse[type]?.toInt()
            if (quickCode != null) {
                stream.writeByte(CLASS_REGISTERED.toInt())
                stream.writeShort(quickCode)
            } else {
                stream.writeByte(NOT_NULL.toInt())
                stream.writeUTF(type.name)
                notCodedSet.add(type.name)
            }
        }
    }

    fun writeNull(stream: DataOutputStream) {
        stream.writeByte(NULL.toInt())
    }

    fun readType(stream: DataInputStream): Class<*>? {
        val startByte = stream.readByte()
        if (startByte == NULL) return null
        if (startByte == NOT_NULL) {
            val name = stream.readUTF()
            notCodedSet.add(name)
            return Class.forName(name)
        }
        if (startByte == CLASS_REGISTERED) return quickCodes[stream.readShort()]
        return null;
    }

    @Suppress("UNCHECKED_CAST") fun <T : Any> writeTyped(stream: DataOutputStream, obj: Any, type: Class<T>) {
        getSerializer(type).write(stream, obj as T)
    }

    inline fun <reified T : Any> readTyped(stream: DataInputStream): T = readTyped(stream, T::class.java)
    @Suppress("UNCHECKED_CAST") fun <T : Any> readTyped(stream: DataInputStream, type: Class<T>): T {
        return getSerializer(type).read(stream)
    }

    inline fun <reified T : Any> readTyped(stream: DataInputStream, item: T): T = readTyped(stream, item, T::class.java)
    @Suppress("UNCHECKED_CAST") fun <T : Any> readTyped(stream: DataInputStream, item: T, type: Class<T>): T {
        return getSerializer(type).read(stream, item)
    }

    @Suppress("UNCHECKED_CAST") fun writeUntyped(stream: DataOutputStream, obj: Any?) {
        if (obj == null) {
            writeNull(stream)
        } else {
            writeType(stream, obj.javaClass)
            writeTyped(stream, obj, obj.javaClass)
        }
    }

    @Suppress("UNCHECKED_CAST") fun readUnyped(stream: DataInputStream): Any? {
        val type = readType(stream) ?: return null
        return readTyped(stream, type)
    }

    @Suppress("UNCHECKED_CAST") fun readUnyped(stream: DataInputStream, item: Any?): Any? {
        val type = readType(stream) ?: return null
        if (item != null && type == item.javaClass) {
            val serializer = getSerializer(type)
            return serializer.readAny(stream, item)
        } else
            return readTyped(stream, type)
    }

    //read and write collections todo
    inline fun <reified T> writeArrayList(stream: DataOutputStream, it: ArrayList<T>) {
        stream.writeInt(it.size)
        for (item in it) {
            writeUntyped(stream, item)
        }
    }

    inline fun <reified T> readArrayList(stream: DataInputStream): ArrayList<T> {
        val new = ArrayList<T>()
        for (i in 1..stream.readInt()) {
            val item = stream.readUntyped()
            if (item is T) {
                new.add(item)
            }
        }
        return new
    }

    inline fun <reified T> readArrayList(stream: DataInputStream, list: ArrayList<T>): ArrayList<T> {
        repeat(stream.readInt()) {
            val current = if(it < list.size) list[it] else null
            val item = stream.readUntyped(current)
            if (item is T) {
                list[it] = item
            }
        }
        return list
    }

    inline fun <reified T : Any> writeArrayListDirect(stream: DataOutputStream, it: ArrayList<T>) {
        stream.writeInt(it.size)
        for (item in it) {
            writeTyped(stream, item, T::class.java)
        }
    }

    inline fun <reified T : Any> readArrayListDirect(stream: DataInputStream): ArrayList<T> {
        val new = ArrayList<T>()
        for (i in 1..stream.readInt()) {
            val item = stream.readTyped(T::class.java)
            new.add(item)
        }
        return new
    }

    inline fun <reified T> writeLinkedList(stream: DataOutputStream, it: LinkedList<T>) {
        stream.writeInt(it.size)
        for (item in it) {
            writeUntyped(stream, item)
        }
    }

    inline fun <reified T> readLinkedList(stream: DataInputStream): LinkedList<T> {
        val new = LinkedList<T>()
        for (i in 1..stream.readInt()) {
            val item = stream.readUntyped()
            if (item is T) {
                new.add(item)
            }
        }
        return new
    }

    inline fun <reified T> readLinkedList(stream: DataInputStream, it: LinkedList<T>): LinkedList<T> {
        it.clear()
        for (i in 1..stream.readInt()) {
            val item = stream.readUntyped()
            if (item is T) {
                it.add(item)
            }
        }
        return it
    }

    inline fun <reified T : Any> writeLinkedListDirect(stream: DataOutputStream, it: LinkedList<T>) {
        stream.writeInt(it.size)
        for (item in it) {
            writeTyped(stream, item, T::class.java)
        }
    }

    inline fun <reified T : Any> readLinkedListDirect(stream: DataInputStream): LinkedList<T> {
        val new = LinkedList<T>()
        for (i in 1..stream.readInt()) {
            val item = stream.readTyped(T::class.java)
            new.add(item)
        }
        return new
    }

    inline fun <reified K : Any, reified T : Any?> writeHashMap(stream: DataOutputStream, it: HashMap<K, T>) {
        stream.writeInt(it.size)
        for ((key, value) in it) {
            writeTyped(stream, key, K::class.java)
            writeUntyped(stream, value)
        }
    }

    inline fun <reified K : Any, reified T : Any?> readHashMap(stream: DataInputStream): HashMap<K, T> {
        val map = HashMap<K, T>()
        for (i in 1..stream.readInt()) {
            map[readTyped(stream, K::class.java)] = readUnyped(stream) as T
        }
        return map
    }

    inline fun <reified K : Any, reified T : Any?> readHashMap(stream: DataInputStream, map: HashMap<K, T>): HashMap<K, T> {
        val removed = HashSet<K>()
        for ((key, value) in map) {
            removed.add(key)
        }
        for (i in 1..stream.readInt()) {
            val key = readTyped(stream, K::class.java)
            removed.remove(key)
            val oldObj = map[key]
            if (oldObj != null) {
                stream.readUntyped(oldObj)
            } else {
                val newObj = stream.readUntyped() as? T
                if (newObj != null) {
                    map.put(key, newObj)
                }
            }
        }
        for (key in removed) {
            map.remove(key)
        }
        return map
    }

    inline fun <reified K : Any> readHashMapUntyped(stream: DataInputStream, map: HashMap<K, *>): HashMap<K, *> {
        val removed = HashSet<K>()
        for ((key, value) in map) {
            removed.add(key)
        }
        for (i in 1..stream.readInt()) {
            val key = readTyped(stream, K::class.java)
            removed.remove(key)
            val oldObj = map[key]
            if (oldObj != null) {
                stream.readUntyped(oldObj)
            } else {
                val newObj = stream.readUntyped()
                if (newObj != null) {
                    map.putAll(mapOf(key to newObj))
                }
            }
        }
        for (key in removed) {
            map.remove(key)
        }
        return map
    }

    //Making and adding new serializers

    fun printNeededQuickCodes() {
        var nextAvailableCode: Short = 0
        for (notCoded in notCodedSet) {
            while (true) {
                val qcClass = quickCodes[nextAvailableCode]
                if (qcClass == null || qcClass.name != notCoded) break
                nextAvailableCode++
            }
            println("Serialize.registerQuickCode<${notCoded.replace('$', '.')}>($nextAvailableCode)")
            nextAvailableCode++
        }
    }

    inline fun <reified T : Any> registerQuickCode(value: Short) = registerQuickCode(T::class.java, value)
    fun registerQuickCode(type: Class<*>, value: Short) {
        quickCodes[value] = type
        quickCodesReverse[type] = value
    }

    fun <T : ISerializer<*>> addSerializer(serializer: T, type: Class<*>): T {
        serializers[type] = serializer
        return serializer
    }

    fun <T : ISerializer<*>> addSerializer(serializer: T): T {
        serializers[serializer.type] = serializer
        return serializer
    }

    inline fun <reified T : Any> serializer(
            crossinline writeFun: DataOutputStream.(item: T) -> Unit,
            crossinline readFun: DataInputStream.() -> T,
            crossinline readIntoFun: DataInputStream.(item: T) -> T
    ) {
        val type = T::class.java
        val s = object : ISerializer<T> {
            override val type: Class<T> = type

            override fun write(stream: DataOutputStream, item: T) {
                stream.writeFun(item)
            }

            override fun read(stream: DataInputStream): T {
                return stream.readFun()
            }

            override fun read(stream: DataInputStream, item: T): T {
                return stream.readIntoFun(item)
            }
        }
        Serialize.addSerializer(s)
    }

    inline fun <reified T : Any> atomicSerializer(
            crossinline writeFun: DataOutputStream.(item: T) -> Unit,
            crossinline readFun: DataInputStream.() -> T
    ) {
        val type = T::class.java
        val s = object : ISerializer<T> {
            override val type: Class<T> = type

            override fun write(stream: DataOutputStream, item: T) {
                stream.writeFun(item)
            }

            override fun read(stream: DataInputStream): T {
                return stream.readFun()
            }

            override fun read(stream: DataInputStream, item: T): T {
                return stream.readFun()
            }
        }
        Serialize.addSerializer(s)
    }

    inline fun <reified T : Any> objectSerializer(
            crossinline writeFun: DataOutputStream.(item: T) -> Unit,
            crossinline readIntoFun: DataInputStream.(item: T) -> T
    ) {
        val type = T::class.java
        val s = object : ISerializer<T> {
            override val type: Class<T> = type

            override fun write(stream: DataOutputStream, item: T) {
                stream.writeFun(item)
            }

            override fun read(stream: DataInputStream): T {
                val new = type.newInstance()
                return stream.readIntoFun(new)
            }

            override fun read(stream: DataInputStream, item: T): T {
                return stream.readIntoFun(item)
            }
        }
        Serialize.addSerializer(s)
    }

    inline fun <reified T : Any> subclassSerializer(
            crossinline writeFun: DataOutputStream.(item: T) -> Unit,
            crossinline readIntoFun: DataInputStream.(item: T) -> T
    ) {
        val type = T::class.java
        val subclassSerializer = getSerializerAny(type.superclass)
        val s = object : ISerializer<T> {
            override val type: Class<T> = type

            override fun write(stream: DataOutputStream, item: T) {
                subclassSerializer.writeAny(stream, item)
                stream.writeFun(item)
            }

            override fun read(stream: DataInputStream): T {
                return stream.readIntoFun(subclassSerializer.read(stream) as T)
            }

            override fun read(stream: DataInputStream, item: T): T {
                return stream.readIntoFun(subclassSerializer.readAny(stream, item) as T)
            }
        }
        Serialize.addSerializer(s)
    }

    init {
        atomicSerializer({ writeUTF(it) }, { readUTF() })

        atomicSerializer({ writeLong(it) }, { readLong() })
        atomicSerializer({ writeInt(it) }, { readInt() })
        atomicSerializer({ writeShort(it.toInt()) }, { readShort() })
        atomicSerializer({ writeByte(it.toInt()) }, { readByte() })

        atomicSerializer({ writeDouble(it) }, { readDouble() })
        atomicSerializer({ writeFloat(it) }, { readFloat() })

        atomicSerializer({ writeBoolean(it) }, { readBoolean() })

        serializer({
            writeFloat(it.x)
            writeFloat(it.y)
        }, {
            Vector2(readFloat(), readFloat())
        }, {
            it.x = readFloat()
            it.y = readFloat()
            it
        })
        serializer({
            writeFloat(it.x)
            writeFloat(it.y)
            writeFloat(it.z)
        }, {
            Vector3(readFloat(), readFloat(), readFloat())
        }, {
            it.x = readFloat()
            it.y = readFloat()
            it.z = readFloat()
            it
        })
        serializer({
            writeFloat(it.r)
            writeFloat(it.g)
            writeFloat(it.b)
            writeFloat(it.a)
        }, {
            Color(readFloat(), readFloat(), readFloat(), readFloat())
        }, {
            it.r = readFloat()
            it.g = readFloat()
            it.b = readFloat()
            it.a = readFloat()
            it
        })
    }
}

fun DataOutputStream.writeTyped(obj: Any) = Serialize.writeTyped(this, obj, obj.javaClass)
inline fun <reified T : Any> DataInputStream.readTyped(): T = Serialize.readTyped(this)
inline fun <reified T : Any> DataInputStream.readTyped(obj: T): T = Serialize.readTyped(this, obj)
fun <T : Any> DataInputStream.readTyped(type: Class<T>): T = Serialize.readTyped(this, type)
fun <T : Any> DataInputStream.readTyped(type: Class<T>, obj: T): T = Serialize.readTyped(this, obj, type)

fun DataOutputStream.writeUntyped(obj: Any?) = Serialize.writeUntyped(this, obj)
fun DataInputStream.readUntyped(): Any? = Serialize.readUnyped(this)
fun DataInputStream.readUntyped(obj: Any?): Any? = Serialize.readUnyped(this, obj)

fun OutputStream.writeUntyped(obj: Any?) = Serialize.writeUntyped(DataOutputStream(this), obj)
fun InputStream.readUntyped(): Any? = Serialize.readUnyped(DataInputStream(this))
fun InputStream.readUntyped(obj: Any?): Any? = Serialize.readUnyped(DataInputStream(this), obj)

inline fun <reified T> OutputStream.writeArrayList(obj: ArrayList<T>) = Serialize.writeArrayList(DataOutputStream(this), obj)
inline fun <reified T> InputStream.readArrayList(): ArrayList<T> = Serialize.readArrayList(DataInputStream(this))
inline fun <reified T> InputStream.readArrayList(obj: ArrayList<T>): ArrayList<T> = Serialize.readArrayList(DataInputStream(this), obj)

inline fun <reified T : Any> OutputStream.writeArrayListDirect(obj: ArrayList<T>) = Serialize.writeArrayListDirect(DataOutputStream(this), obj)
inline fun <reified T : Any> InputStream.readArrayListDirect(): ArrayList<T> = Serialize.readArrayListDirect<T>(DataInputStream(this))

inline fun <reified T> OutputStream.writeLinkedList(obj: LinkedList<T>) = Serialize.writeLinkedList(DataOutputStream(this), obj)
inline fun <reified T> InputStream.readLinkedList(): LinkedList<T> = Serialize.readLinkedList(DataInputStream(this))
inline fun <reified T> InputStream.readLinkedList(obj: LinkedList<T>): LinkedList<T> = Serialize.readLinkedList(DataInputStream(this), obj)

inline fun <reified T : Any> OutputStream.writeLinkedListDirect(obj: LinkedList<T>) = Serialize.writeLinkedListDirect(DataOutputStream(this), obj)
inline fun <reified T : Any> InputStream.readLinkedListDirect(): LinkedList<T> = Serialize.readLinkedListDirect<T>(DataInputStream(this))

inline fun <reified K : Any, reified T : Any?> OutputStream.writeHashMap(obj: HashMap<K, T>) = Serialize.writeHashMap(DataOutputStream(this), obj)
inline fun <reified K : Any, reified T : Any?> InputStream.readHashMap(): HashMap<K, T> = Serialize.readHashMap<K, T>(DataInputStream(this))
inline fun <reified K : Any, reified T : Any?> InputStream.readHashMap(map: HashMap<K, T>): HashMap<K, T> = Serialize.readHashMap(DataInputStream(this), map)
inline fun <reified K : Any> InputStream.readHashMapUntyped(map: HashMap<K, *>): HashMap<K, *> = Serialize.readHashMapUntyped(DataInputStream(this), map)