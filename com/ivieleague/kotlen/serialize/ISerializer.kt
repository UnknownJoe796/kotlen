package com.ivieleague.kotlen.serialize

import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * Created by josep on 10/30/2015.
 */
interface ISerializer<T> {
    val type: Class<T>
    fun write(stream: DataOutputStream, item: T): Unit
    fun read(stream: DataInputStream): T
    fun read(stream: DataInputStream, item: T): T
    @Suppress("UNCHECKED_CAST") fun writeAny(stream: DataOutputStream, item: Any?) = write(stream, item as T)
    @Suppress("UNCHECKED_CAST") fun readAny(stream: DataInputStream, item: Any?): Any? = read(stream, item as T)
}