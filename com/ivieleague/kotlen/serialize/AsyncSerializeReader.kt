package com.ivieleague.kotlen.serialize

import java.io.DataInputStream
import java.io.InputStream
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Reads from an input stream asynchronously
 * Created by josep on 11/6/2015.
 */
class AsyncSerializeReader(val inputStream: InputStream) {
    val dataStream: DataInputStream = DataInputStream(inputStream)
    val data: ConcurrentLinkedQueue<Any> = ConcurrentLinkedQueue()

    var onReceive: (Any) -> Boolean = { false }
    var onClosed: () -> Unit = {}

    private var shouldStop: Boolean = false
    val isStopped: Boolean get() = shouldStop

    val thread: Thread = Thread(object : Runnable {
        override fun run() {
            try {
                while (!shouldStop) {
                    try {
                        val value = dataStream.readUntyped()
                        if (value != null && !onReceive(value)) {
                            data.add(value)
                        }
                    } catch(e: Exception) {
                        e.printStackTrace()
                    }
                }
            } catch(e: Exception) {
                e.printStackTrace()
            } finally {
                onClosed()
            }
        }
    })

    init {
        thread.start()
    }

    @Synchronized fun stop() {
        shouldStop = true
        if (thread.isAlive) {
            thread.join(1000)
        }
    }
}