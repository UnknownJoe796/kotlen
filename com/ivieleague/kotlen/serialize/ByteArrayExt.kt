package com.ivieleague.kotlen.serialize

import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream

/**
 * Created by josep on 12/11/2015.
 */
inline fun toByteArray(streamAction: (DataOutputStream) -> Unit): ByteArray {
    val stream = ByteArrayOutputStream()
    val wrapper = DataOutputStream(stream)
    streamAction(wrapper)
    return stream.toByteArray()
}

inline fun ByteArray.toStream(streamAction: (DataInputStream) -> Unit) {
    val stream = ByteArrayInputStream(this)
    val wrapper = DataInputStream(stream)
    streamAction(wrapper)
}