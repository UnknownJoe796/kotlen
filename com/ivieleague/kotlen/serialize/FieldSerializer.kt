package com.ivieleague.kotlen.serialize

import java.io.DataInputStream
import java.io.DataOutputStream
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.util.*

/**
 * Serializes any object by field, caching the serialization references and field references.
 * Created by josep on 10/30/2015.
 */
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class DirectWrite

class FieldSerializer<T : Any>(type: Class<T>) : ISerializer<T> {

    private object anySerializer : ISerializer<Any?> {
        override val type: Class<Any?> get() = throw IllegalAccessException()

        override fun write(stream: DataOutputStream, item: Any?) {
            println(item?.javaClass?.simpleName)
            stream.writeUntyped(item)
        }

        override fun read(stream: DataInputStream): Any? {
            return stream.readUntyped()
        }

        override fun read(stream: DataInputStream, item: Any?): Any? {
            return stream.readUntyped(item)
        }

    }

    override val type: Class<T> = type
    val fieldSerializers: ArrayList<Pair<Field, ISerializer<*>>> = ArrayList()

    init {
        for (field in type.declaredFields) {
            field.isAccessible = true
            if (Modifier.isTransient(field.modifiers)) continue

            if (field.isAnnotationPresent(DirectWrite::class.java)) {
                val newSerializer = Serialize.getSerializer(field.type)
                fieldSerializers.add(field to newSerializer)
            } else {
                fieldSerializers.add(field to anySerializer)
            }
        }
    }

    override fun write(stream: DataOutputStream, item: T) {
        for ((field, serializer) in fieldSerializers) {
            serializer.writeAny(stream, field.get(item))
        }
    }

    override fun read(stream: DataInputStream): T {
        val item = type.newInstance()
        for ((field, serializer) in fieldSerializers) {
            field.set(item, serializer.readAny(stream, field.get(item)))
        }
        return item
    }

    override fun read(stream: DataInputStream, item: T): T {
        for ((field, serializer) in fieldSerializers) {
            field.set(item, serializer.readAny(stream, field.get(item)))
        }
        return item
    }

}