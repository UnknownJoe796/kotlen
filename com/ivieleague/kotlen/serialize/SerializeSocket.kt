package com.ivieleague.kotlen.serialize

import com.badlogic.gdx.net.Socket
import java.io.BufferedOutputStream
import java.io.DataOutputStream
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Created by josep on 11/7/2015.
 */
class SerializeSocket(val socket: Socket) : Socket by socket {
    private val buffer: BufferedOutputStream = BufferedOutputStream(socket.outputStream)
    private val outputStream: DataOutputStream = DataOutputStream(buffer)

    private val inputReader: AsyncSerializeReader = AsyncSerializeReader(socket.inputStream)

    var onReceive: (Any) -> Boolean
        get() = inputReader.onReceive
        set(value) {
            inputReader.onReceive = value
        }
    var onClosed: () -> Unit = {}

    val output: ConcurrentLinkedQueue<Any> get() = inputReader.data

    @Synchronized fun send(obj: Any) {
        if (socket.isConnected == false) return
        println("sending $obj")
        outputStream.writeUntyped(obj)
        buffer.flush()
    }

    @Synchronized fun close() {
        inputReader.onClosed = {
            inputStream.close()
            outputStream.close()
            onClosed()
        }
        inputReader.stop()
    }
}