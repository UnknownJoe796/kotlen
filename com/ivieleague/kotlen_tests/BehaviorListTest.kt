package com.ivieleague.kotlen_tests

import com.ivieleague.kotlen.Behavior
import com.ivieleague.kotlen.BehaviorList
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.*

/**
 * Created by josep on 11/26/2015.
 */
class BehaviorListTest {
    @Before fun setUp() {

    }

    @After fun tearDown() {

    }

    @Test fun orderTest() {
        var list = BehaviorList()
        val startOrder = ArrayList<Int>()
        val stopOrder = ArrayList<Int>()
        val stepOrder = ArrayList<Int>()

        list.add(SingleActionBehavior(
                3,
                { startOrder.add(1) },
                { stepOrder.add(3) },
                { stopOrder.add(5) }
        ))
        list.add(SingleActionBehavior(
                4,
                { startOrder.add(2) },
                { stepOrder.add(4) },
                { stopOrder.add(4) }
        ))
        list.add(SingleActionBehavior(
                1,
                { startOrder.add(3) },
                { stepOrder.add(1) },
                { stopOrder.add(3) }
        ))
        list.add(SingleActionBehavior(
                2,
                { startOrder.add(4) },
                { stepOrder.add(2) },
                { stopOrder.add(2) }
        ))
        list.add(SingleActionBehavior(
                3,
                { startOrder.add(5) },
                { stepOrder.add(3) },
                { stopOrder.add(1) }
        ))

        list.start()
        list.step(1f)
        list.stop()

        var last: Int;

        last = Int.MIN_VALUE
        assert(startOrder.size == 5)
        for (current in startOrder) {
            if (current > last) {
                last = current
            } else {
                error("Wrong startOrder")
            }
        }

        last = Int.MIN_VALUE
        assert(stepOrder.size == 5)
        for (current in stepOrder) {
            if (current >= last) {
                last = current
            } else {
                error("Wrong stepOrder")
            }
        }

        last = Int.MIN_VALUE
        assert(stopOrder.size == 5)
        for (current in stopOrder) {
            if (current > last) {
                last = current
            } else {
                error("Wrong stopOrder")
            }
        }
    }

    class SingleActionBehavior(override val order: Int, val onStart: () -> Unit, val action: () -> Unit, val onStop: () -> Unit) : Behavior {
        override fun start() {
            onStart()
        }

        override fun step(delta: Float) {
            action()
        }

        override fun render() {

        }

        override fun stop() {
            onStop()
        }

        override fun resize(width: Int, height: Int) {
        }

        override fun dispose() {
        }

    }
}